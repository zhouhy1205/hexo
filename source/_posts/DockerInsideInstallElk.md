---
title: Docker中安装ELK
date: 2020-04-11 16:25:10
updated: 2020-04-11 16:25:10
tags: [Elasticsearch,Logstash,Kibana]
categories: [Docker]
description: "在上一篇中介绍了Docker基本情况以及学习Docker的必要性，但是一篇文章看完发现几个指令就安装完了什么也没学到，Docker的优点也没有清晰的感受到，在这篇文章中就通过安装鼎鼎大名的ELK来实际操作一下。"
---

![elk](DockerInsideInstallElk/elk.jpeg)

## 前言

在上一篇中介绍了Docker基本情况以及学习Docker的必要性，但是一篇文章看完发现几个指令就安装完了什么也没学到，Docker的优点也没有清晰的感受到，在这篇文章中就通过安装鼎鼎大名的ELK来实际操作一下。



## 正文

### 介绍

ELK是[Elasticsearch](https://www.elastic.co/cn/)、[Logstash](https://www.elastic.co/cn/)、[Kibana](https://www.elastic.co/cn/)的简称，这三者是核心套件，但并非全部。

**Elasticsearch**

​	Elasticsearch是实时全文搜索和分析引擎，提供搜集、分析、存储数据三大功能；是一套开放REST和JAVA API等结构提供高效搜索功能，可扩展的分布式系统。它构建于Apache Lucene搜索引擎库之上。

**Logstash**

​	Logstash是一个用来搜集、分析、过滤日志的工具。它支持几乎任何类型的日志，包括系统日志、错误日志和自定义应用程序日志。它可以从许多来源接收日志，这些来源包括 syslog、消息传递（例如 RabbitMQ）和JMX，它能够以多种方式输出数据，包括电子邮件、`websockets`和`Elasticsearch`。

**Kibana**

​	Kibana是一个基于Web的图形界面，用于搜索、分析和可视化存储在 Elasticsearch指标中的日志数据。它利用Elasticsearch的REST接口来检索数据，不仅允许用户创建他们自己的数据的定制仪表板视图，还允许他们以特殊的方式查询和过滤数据。



**ElasticSearch-Head**

​	ElasticSearch-Head不属于ELK中的组件，而是第三方开发并且已经停止维护的中间件，为什么要安装`ElasticSearch-Head`呢，原因是需要有一个管理界面进行查看`ElasticSearch`相关信息



### 流程

**Logstash**从各个地方读取日志后按配置的规则去过滤一遍在推送到**Elasticsearch**,这个时候就可以通过**Kibana**对数据进行搜索、分析操作等。

![elk流程](DockerInsideInstallElk/elk流程.jpg)



### 安装

**友情提醒**：ES和kibana的版本尽可能的保证一致，否则要去修改很多配置信息，而且不一定能安装成功，现象：ES安装成功了但是kibana无法链接到ES，报红（red）。

```
#在docker中拉取镜像
docker pull elasticsearch:7.8.0
docker pull logstash:7.8.0
docker pull kibana:7.8.0
#elasticsearch-head 可安装可不安装
docker pull mobz/elasticsearch-head:5
```

#### 配置

进入镜像中修改配置文件

**kibana.yml**

```
server.name: kibana
server.host: 0.0.0.0
elasticsearch.hosts: [ "http://192.168.1.103:9200" ]

#monitoring.ui.container.elasticsearch.enabled: true
### X-Pack security credentials
#elasticsearch.username: elastic
#elasticsearch.password: changeme
```

**/usr/share/logstash/config文件夹下创建logstash.yml**

```
http.host: "0.0.0.0"
xpack.monitoring.elasticsearch.hosts: [ "http://192.168.1.103:9200" ]
#elasticsearch 这里写的是你的ip
## X-Pack security credentials
#xpack.monitoring.enabled: true
#xpack.monitoring.elasticsearch.username: elastic
#xpack.monitoring.elasticsearch.password: changeme
```

**/usr/share/logstash/config文件夹下创建pipeline.yml**

```
- pipeline.id: main
  path.config: "/usr/share/logstash/pipeline"
```

**/usr/share/logstash下创建pipeline目录**

这里只是以Mysql为例，logstash支持几乎任何类型的日志。

**pipeline下创建mysql.conf **

```
input {
  jdbc {
    jdbc_driver_library => "/usr/share/logstash/mysql-connector-java-5.1.46.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    jdbc_connection_string => "jdbc:mysql://192.168.1.103:3306/iwarning"
    jdbc_user => "root"
    jdbc_password => "123456"
    schedule => "* * * * *"
    statement => "SELECT * FROM dataconfig>= :sql_last_value"
    use_column_value => true
    tracking_column_type => "numeric"
    tracking_column => "id"
    last_run_metadata_path => "syncpoint_table"
  }
}

output {
    elasticsearch {
        # ES的IP地址及端口
        hosts => ["192.168.1.103:9200"]
        # 索引名称 可自定义
        index => "dataconfig"
        # 需要关联的数据库中有有一个id字段，对应类型中的id
        document_id => "%{id}"
        document_type => "dataconfig"
    }
    stdout {
        # JSON格式输出
        codec => json_lines
    }
}
```

#### 启动

```
#"discovery.type=single-node" 单节点启动
docker run -d --name elk_es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.8.0

docker run -d --name elk_k -p 5601:5601 --link elk_es:elasticsearch kibana:7.8.0

docker run -d --name elk_l -p 5044:5044 -p 9600:9600 logstash:7.8.0

docker run -d --name es_admin -p 9100:9100 mobz/elasticsearch-head:5
```



#### 检查

**elasticsearch**

http://localhost:9200/

![es启动成功界面](DockerInsideInstallElk/es启动成功界面.png)

**kibana**

http://localhost:5601

![kibana启动成功界面](DockerInsideInstallElk/kibana启动成功界面.png)

**elasticsearch-head**

http://localhost:9100/

![es_head启动成功界面](DockerInsideInstallElk/es_head启动成功界面.png)

### 遇到的错误

#### 错误1

```
max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```

 **解决办法**：
1、切换到root用户修改配置sysctl.conf

```
vi /etc/sysctl.conf 
```

添加下面配置：

```
vm.max_map_count=262144
```

并执行命令：

```
sysctl -p
```

然后，重新启动elasticsearch，即可启动成功。



#### 错误2

这个错误我忘记截图了，大概意思是logstash连接数据库失败，是因为之前同一IP连接数据库失败次数超过mysql的默认的最大错误次数。



加大mysql的全局最大连接错误数就可以了。

```
set global max_connect_errors = 10000
```





### 总结

​	在Docker中安装东西实在是太方便了，而且还避免了很多不必要的错误，当然这还不是最快的，后面会介绍**Docker Compose** ，在单个机器中安装elk看起来很简单，如果是100台、10000台呢？Docker的服务编排就很好的解决了这个问题。



这篇文章主要记录在Docker中安装Elk，关于Elk的简单使用在后面结合ELK集群再补充。



