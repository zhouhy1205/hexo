---
title: Linux开发环境搭建
date: 2018-09-16 14:03:04
updated: 2018-09-16 14:03:04
tags: [Dev,Navicat,MySQL,Postman,Python,Pyenv]
categories: Linux
description: "因为最近重装系统比较频繁，经常需要搭建开发环境，虽然搭建环境很简单但是比较繁琐，一些配置的字符串还需要google一下，太耽误时间，所以记录一下基本的开发环境搭建和常用软件。"
---

## 前言

因为最近重装系统比较频繁，经常需要搭建开发环境，虽然搭建环境很简单但是比较繁琐，一些配置的字符串还需要google一下，太耽误时间，所以记录一下基本的开发环境搭建和常用软件。

## 正文

### Jdk

#### 下载

在Oracle官网[下载JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

Oracle 账号

```
4670049@qq.com
1QAZ2wsx!
```

注意：这里需要下载 Linux 版本。这里以jdk-8u231-linux-x64.tar.gz为例，你下载的文件可能不是这个版本，这没关系，只要后缀(.tar.gz)一致即可。



#### 创建目录

在`/usr/`目录下创建`java`目录，

```
mkdir /usr/java
cd /usr/java
```

把下载的文件 jdk-8u231-linux-x64.tar.gz 放在/usr/java/目录下。

#### 解压 JDK

```
tar -zxvf jdk-8u231-linux-x64.tar.gz
```

#### 设置环境变量

```
修改 /etc/profile
```

在 profile 文件中添加如下内容并保存：

```
#set java environment
JAVA_HOME=/usr/java/jdk1.8.0_151        
JRE_HOME=/usr/java/jdk1.8.0_151/jre     
CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
export JAVA_HOME JRE_HOME CLASS_PATH PATH
```

 注意：其中 JAVA_HOME， JRE_HOME 请根据自己的实际安装路径及 JDK 版本配置。

让修改生效：

```
source /etc/profile
```

#### 测试

```
java -version
```

显示 java 版本信息，则说明 JDK 安装成功：

```
java version "1.8.0_231"
Java(TM) SE Runtime Environment (build 1.8.0_151-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)
```



### Maven

#### 下载

[下载地址](https://mirrors.cnnic.cn/apache/maven/)

此处使用wget下载二进制包

```
wget https://mirrors.cnnic.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz --no-check-certificate
```

#### 解压安装

```
tar -xf apache-maven-3.6.3-bin.tar.gz 
mv apache-maven-3.6.3 /usr/local/maven 				#移动
ln -s /usr/local/maven/bin/mvn  /usr/bin/mvn　　　　 # 与jenkins联合使用时，jenkins会到/usr/bin/下找mvn命令，如果没有回报错
ll /usr/local/maven/
ll /usr/bin/mvn
```

#### 配置环境变量

**配置全局环境变量**

`vi /etc/profile` 

配置内容

```
export MAVEN_HOME=/app/apache-maven-3.6.3
export PATH=$MAVEN_HOME/bin:$PATH
```

刷新

```
source /etc/profile
echo $PATH
```



#### 查看版本

```
which mvn
mvn -version
```

最后可以看到以下结果

```
[root@CentOS75 conf]# which mvn　　　　　　# 查看mvn工具的位置/usr/local/maven/bin/mvn
[root@CentOS75 conf]# mvn -version　　　　# 查看maven版本
Apache Maven 3.5.4 (1edded0938998edf8bf061f1ceb3cfdeccf443fe; 2018-06-18T02:33:14+08:00)
Maven home: /usr/local/maven
Java version: 1.8.0_231, vendor: Oracle Corporation, runtime: /usr/java/jdk1.8.0_181-amd64/jre
Default locale: zh_CN, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-862.11.6.el7.x86_64", arch: "amd64", family: "unix"
```



### Mysql

#### 系统约定

安装文件下载目录：`/data/software` 

Mysql目录安装位置：`/usr/local/mysql` 

数据库保存位置：`/data/mysql` 

日志保存位置：`/data/log/mysql`

这些配置可以在my.cnf文件中配置，my.cnf文件可以在多个位置，

可以用过以下命令查看

```
/usr/local/mysql/bin/mysqld --verbose --help |grep -A 1 'Default options'
```

默认读取顺序依次是

```
Default options are read from the following files in the given order:
/etc/my.cnf /etc/mysql/my.cnf /usr/local/mysql/etc/my.cnf ~/.my.cnf 
```

#### 下载

根据自己的需要下载相对应的版本[下载](https://dev.mysql.com/downloads/mysql/)



#### 解压移动

```
sudo tar -xvJf mysql-8.0.18-linux-glibc2.12-x86_64.tar.xz -C /usr/local 
```



#### 创建软链接

```
cd /usr/local
sudo ln -s mysql-8.0.18-linux-glibc2.12-x86_64 mysql
```



#### 授权

```
sudo chown -R zhou:zhou mysql
```



#### 安装

cd /usr/local/mysql

```
sudo /usr/local/mysql/bin/mysqld --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --initialize
```

正常安装之后会显示如下结果：

```
sudo ./bin/mysqld --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --initialize
2018-09-19T16:31:03.993062Z 0 [System][MY-013169] [Server] /usr/local/mysql-8.0.12-linux-glibc2.12-x86_64/bin/mysqld (mysqld 8.0.12) initializing of server in progress as process 19261
2018-09-19T16:31:19.499443Z 5 [Note][MY-010454] [Server] A temporary password is generated for root@localhost: ;f;#<N6nl7pE
```

记下随机产生的密码，我的是;**f;#<N6nl7pE****



#### 添加配置

在/etc/mysql/my.cnf中添加相关配置

```
[mysqld]
user = zhou
basedir = /usr/local/mysql
datadir = /usr/local/mysql/data
log-error = /usr/local/mysql/data/error.log
pid-file = /usr/local/mysql/mysql.pid
tmpdir = /usr/local/mysql/tmp
socket= /tmp/mysql.sock
```



#### 开启mysql服务

```
sudo /usr/local/mysql/support-files/mysql.server start
```

出现黄色OK字样则启动完成。



#### 自启动

```
sudo cp support-files/mysql.server /etc/init.d/mysqld
```



#### 相关命令

```
service mysqld restart #重启
service mysqld start #启动
service mysqld restop #停止
```



#### 软链接

```
sudo ln -s /usr/local/mysql/bin/mysql /usr/bin #软链接
mysql -u root -p  #登陆 输入第6步记下的密码
alter user 'root'@'localhost' identified by '123456';  # 设置密码
```



#### 忘记密码

如果忘记密码可以通过以下方式解决。

关闭实例，创建一个sql文件

写上密码修改语句

```
# vi init.sql 
alter user 'root'@'localhost' identified by '123456';
```

 最后，使用--init-file参数，启动实例

```
# /usr/local/mysql/bin/mysqld_safe --defaults-file=my.cnf --init-file=/usr/local/mysql/init.sql &
```

实例启动成功后，密码即修改完毕~

 如果mysql实例是通过服务脚本来管理的，除了创建sql文件，整个操作可简化为一步。

```
# service mysqld restart --init-file=/usr/local/mysql/init.sql 
```

**注意：该操作只适用于/etc/init.d/mysqld这种服务管理方式，不适用于RHEL 7新推出的systemd。**





### Navicat

#### 下载

直接在[官网](https://www.navicat.com.cn/)下载。

#### 安装

下载之后可以看到文件是`AppImage`格式的

```
navicat15-premium-cs.AppImage
```

直接赋予运行权限，即可运行。

```
chmod +x navicat15-premium-cs.AppImage

./navicat15-premium-cs.AppImage
```

#### 破解

**提取AppImage**

假定这个AppImage文件在 `~/Desktop` 文件夹下

```
mkdir ~/Desktop/navicat15-premium-en
sudo mount -o loop ~/Desktop/navicat15-premium-en.AppImage ~/Desktop/navicat15-premium-en
cp -r ~/Desktop/navicat15-premium-en ~/Desktop/navicat15-premium-en-patched
sudo umount ~/Desktop/navicat15-premium-en
rm -rf ~/Desktop/navicat15-premium-en
```

**编译**

编译patcher和keygen

**前提条件**

1. 请确保你安装了下面几个库：

   - `capstone`
   - `keystone`
   - `rapidjson`

   你可以通过下面的命令来安装它们：

   ```
   # install capstone
   $ sudo apt-get install libcapstone-dev
   
   # install keystone
   $ sudo apt-get install cmake
   $ git clone https://github.com/keystone-engine/keystone.git
   $ cd keystone
   $ mkdir build
   $ cd build
   $ ../make-share.sh
   $ sudo make install
   $ sudo ldconfig
   
   # install rapidjson
   $ sudo apt-get install rapidjson-dev
   ```

2. **你的gcc支持C++17特性。（注意）**

**开始编译**

```
$ git clone -b linux --single-branch https://github.com/DoubleLabyrinth/navicat-keygen.git
$ cd navicat-keygen
$ make all
```

生成完成后，你会在 `bin/` 文件夹下看到编译后的keygen/patcher。

**替换官方公钥**

使用navicat-patcher替换官方公钥

```
Usage:
    navicat-patcher [--dry-run] <Navicat Installation Path> [RSA-2048 Private Key File]

        [--dry-run]                   Run patcher without applying any patches.
                                      This parameter is optional.

        <Navicat Installation Path>   Path to a directory where Navicat locates
                                      This parameter must be specified.

        [RSA-2048 Private Key File]   Path to a PEM-format RSA-2048 private key file.
                                      This parameter is optional.
```

**例如：**

```
$ ./bin/navicat-patcher ~/Desktop/navicat15-premium-en-patched

```

**Navicat Premium 15.0.3 Linux 英文版** 已经通过测试。

下面是一份样例输出：

```
**********************************************************
*       Navicat Patcher (Linux) by @DoubleLabyrinth      *
*                  Version: 1.0                          *
**********************************************************

Press ENTER to continue or Ctrl + C to abort.

[+] Try to open libcc.so ... Ok!

[+] PatchSolution0 ...... Ready to apply
   RefSegment      =  1
   MachineCodeRva  =  0x0000000001413e10
   PatchMarkOffset = +0x00000000029ecf40

[*] Generating new RSA private key, it may take a long time...
[*] Your RSA private key:
   -----BEGIN RSA PRIVATE KEY-----
   MIIEowIBAAKCAQEArRsg1+6JZxZNMhGyuM8d+Ue/ky9LSv/XyKh+wppQMS5wx7QE
   XFcdDgaByNZeLMenh8sgungahWbPo/5jmkDuuHHrVMU748q2JLL1E3nFraPZqoRD
   ...
   ...
   B1Z5AoGBAK8cWMvNYf1pfQ9w6nD4gc3NgRVYLctxFLmkGylqrzs8faoLLBkFq3iI
   s2vdYwF//wuN2aq8JHldGriyb6xkDjdqiEk+0c98LmyKNmEVt8XghjrZuUrn8dA0
   0hfInLdRpaB7b+UeIQavw9yLH0ilijAcMkGzzom7vdqDPizoLpXQ
   -----END RSA PRIVATE KEY-----
[*] Your RSA public key:
   -----BEGIN PUBLIC KEY-----
   MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArRsg1+6JZxZNMhGyuM8d
   +Ue/ky9LSv/XyKh+wppQMS5wx7QEXFcdDgaByNZeLMenh8sgungahWbPo/5jmkDu
   ...
   ...
   GrVJ3o8aDm35EzGymp4ON+A0fdAkweqKV6FqxEJqLWIDRYh+Z01JXUZIrKmnCkgf
   QQIDAQAB
   -----END PUBLIC KEY-----

*******************************************************
*                   PatchSolution0                    *
*******************************************************
[*] Previous:
+0x0000000000000070                          01 00 00 00 05 00 00 00          ........
+0x0000000000000080  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
+0x0000000000000090  00 00 00 00 00 00 00 00 40 cf 9e 02 00 00 00 00  ........@.......
+0x00000000000000a0  40 cf 9e 02 00 00 00 00 00 10 00 00 00 00 00 00  @...............
[*] After:
+0x0000000000000070                          01 00 00 00 05 00 00 00          ........
+0x0000000000000080  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
+0x0000000000000090  00 00 00 00 00 00 00 00 d0 d0 9e 02 00 00 00 00  ................
+0x00000000000000a0  d0 d0 9e 02 00 00 00 00 00 10 00 00 00 00 00 00  ................

[*] Previous:
+0x00000000029ecf40  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
+0x00000000029ecf50  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
+0x00000000029ecf60  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
...
...
+0x00000000029ed0c0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
[*] After:
+0x00000000029ecf40  ef be ad de 4d 49 49 42 49 6a 41 4e 42 67 6b 71  ....MIIBIjANBgkq
+0x00000000029ecf50  68 6b 69 47 39 77 30 42 41 51 45 46 41 41 4f 43  hkiG9w0BAQEFAAOC
+0x00000000029ecf60  41 51 38 41 4d 49 49 42 43 67 4b 43 41 51 45 41  AQ8AMIIBCgKCAQEA
...
...
...
+0x00000000029ed0c0  43 6b 67 66 51 51 49 44 41 51 41 42 ad de ef be  CkgfQQIDAQAB....

[*] Previous:
+0x0000000001413e10  44 0f b6 24 18 48 8b 44 24 28 8b 50 f8 85 d2 79  D..$.H.D$(.P...y
+0x0000000001413e20  6f                                               o               
[*] After:
+0x0000000001413e10  45 31 e4 48 8d 05 2a 91 5d 01 90 90 90 90 90 90  E1.H..*.].......
+0x0000000001413e20  90                                               .               

[*] New RSA-2048 private key has been saved to
   /home/doublesine/github.com/navicat-keygen/RegPrivateKey.pem

*******************************************************
*           PATCH HAS BEEN DONE SUCCESSFULLY!         *
*                  HAVE FUN AND ENJOY~                *
*******************************************************

```

**重新打包**

重新打包成成AppImage文件

**例如：**

```
$ wget 'https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage'
$ chmod +x appimagetool-x86_64.AppImage
$ ./appimagetool-x86_64.AppImage ~/Desktop/navicat15-premium-en-patched ~/Desktop/navicat15-premium-en-patched.AppImage

```

**运行刚生成的AppImage**

```
$ chmod +x ~/Desktop/navicat15-premium-en-patched.AppImage
$ ~/Desktop/navicat15-premium-en-patched.AppImage

```

**生成序列号和激活码**

使用 `navicat-keygen` 来生成 **序列号** 和 **激活码**

```
Usage:
    navicat-keygen <--bin|--text> [--adv] <RSA-2048 Private Key File>

        <--bin|--text>    Specify "--bin" to generate "license_file" used by Navicat 11.
                          Specify "--text" to generate base64-encoded activation code.
                          This parameter must be specified.

        [--adv]                       Enable advance mode.
                                      This parameter is optional.

        <RSA-2048 Private Key File>   A path to an RSA-2048 private key file.
                                      This parameter must be specified.

```

**例如：**

```
$ ./bin/navicat-keygen --text ./RegPrivateKey.pem

```

你会被要求选择Navicat产品类别、Navicat语言版本和填写主版本号。之后一个随机生成的 **序列号** 将会给出。

```
$ ./bin/navicat-keygen --text ./RegPrivateKey.pem
**********************************************************
*       Navicat Keygen (Linux) by @DoubleLabyrinth       *
*                   Version: 1.0                         *
**********************************************************

[*] Select Navicat product:
0. DataModeler
1. Premium
2. MySQL
3. PostgreSQL
4. Oracle
5. SQLServer
6. SQLite
7. MariaDB
8. MongoDB
9. ReportViewer

(Input index)> 1

[*] Select product language:
0. English
1. Simplified Chinese
2. Traditional Chinese
3. Japanese
4. Polish
5. Spanish
6. French
7. German
8. Korean
9. Russian
10. Portuguese

(Input index)> 0

[*] Input major version number:
(range: 0 ~ 15, default: 12)> 15

[*] Serial number:
NAVM-RTVJ-EO42-IODD

[*] Your name:

```

你可以使用这个 **序列号** 来暂时激活Navicat。

之后你会被要求填写 **用户名** 和 **组织名**。你可以随意填写，但别太长。

```
[*] Your name: DoubleLabyrinth
[*] Your organization: DoubleLabyrinth

[*] Input request code in Base64: (Double press ENTER to end)

```

之后你会被要求填写请求码。**注意不要关闭keygen。**

**激活**

**断开网络**. 找到注册窗口，填写keygen给你的 **序列号**，然后点击 `激活`

通常在线激活会失败，所以在弹出的提示中选择 `手动激活`。

复制 **请求码** 到keygen，连按两次回车结束。

```
[*] Input request code in Base64: (Double press ENTER to end)
OaGPC3MNjJ/pINbajFzLRkrV2OaSXYLr2tNLDW0fIthPOJQFXr84OOroCY1XN8R2xl2j7epZ182PL6q+BRaSC6hnHev/cZwhq/4LFNcLu0T0D/QUhEEBJl4QzFr8TlFSYI1qhWGLIxkGZggA8vMLMb/sLHYn9QebBigvleP9dNCS4sO82bilFrKFUtq3ch8r7V3mbcbXJCfLhXgrHRvT2FV/s1BFuZzuWZUujxlp37U6Y2PFD8fQgsgBUwrxYbF0XxnXKbCmvtgh2yaB3w9YnQLoDiipKp7io1IxEFMYHCpjmfTGk4WU01mSbdi2OS/wm9pq2Y62xvwawsq1WQJoMg==

[*] Request Info:
{"K":"NAVMRTVJEO42IODD", "DI":"4A12F84C6A088104D23E", "P":"linux"}

[*] Response Info:
{"K":"NAVMRTVJEO42IODD","DI":"4A12F84C6A088104D23E","N":"DoubleLabyrinth","O":"DoubleLabyrinth","T":1575543648}

[*] Activation Code:
i45HIr7T1g69Cm9g3bN1DBpM/Zio8idBw3LOFGXFQjXj0nPfy9yRGuxaUBQkWXSOWa5EAv7S9Z1sljlkZP6cKdfDGYsBb/4N1W5Oj1qogzNtRo5LGwKe9Re3zPY3SO8RXACfpNaKjdjpoOQa9GjQ/igDVH8r1k+Oc7nEnRPZBm0w9aJIM9kS42lbjynVuOJMZIotZbk1NloCodNyRQw3vEEP7kq6bRZsQFp2qF/mr+hIPH8lo/WF3hh+2NivdrzmrKKhPnoqSgSsEttL9a6ueGOP7Io3j2lAFqb9hEj1uC3tPRpYcBpTZX7GAloAENSasFwMdBIdszifDrRW42wzXw==

```

1. 最终你会得到一个base64编码的 **激活码**。

   将之复制到 `手动激活` 的窗口，然后点击 `激活`。

   如果没有什么意外，应该可以成功激活。

2. 最后的清理：

   ```
   $ rm ~/Desktop/navicat15-premium-en.AppImage
   $ rm -rf ~/Desktop/navicat15-premium-en-patched
   $ mv ~/Desktop/navicat15-premium-en-patched.AppImage ~/Desktop/navicat15-premium-en.AppImage
   
   ```



### Pyenv

**默认路径**

pyenv默认安装路径为当前用户下的.pyenv文件夹下l,

例如：

```
/home/zhou/.pyenv # pyenv 安装路径

/home/zhou/.pyenv/versions/ #python安装路径

```



#### 安装pyenv

方法一：

安装

```
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash

```

安装完成后，将输出的最后的三行追加到/etc/profile,做环境变量

```
# Load pyenv automatically by adding
# the following to ~/.bash_profile:

export PATH="/home/zhou/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

```

方法二：

安装

```
git clone https://github.com/yyuu/pyenv.git ~/.pyenv

```

环境变量：

```
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> /etc/profile  
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> /etc/profile
echo 'eval "$(pyenv init -)"' >> /etc/profile

```



#### 使用变量

```
source /etc/profile

```



#### Pyenv相关命令

```
查看可安装的版本
pyenv install –list

安装Python
pyenv install -v 3.7.5

卸载Python
pyenv uninstall 2.7.13

查看pyenv版本
pyenv version

查看已经安装的python版本
pyenv versions

切换Python版本
pyenv global 3.6.4


```





### Python

#### 安装

查看可安装的版本

```
pyenv install –list

```

如果可以访问外网并且list中有自己需要的版本直接执行**安装命令**。



国内加速方法1：

```
v=3.7.5;wget http://npm.taobao.org/mirrors/python/$v/Python-$v.tar.xz -P ~/.pyenv/cache/;pyenv install $v

```

国内加速方法2：

如果没有想要安装的版本或者下载Python版本超时，可以手动下载后放置在~/.pyenv/cache/直接执行以下命令。



安装

`pyenv install -v 3.7.5



如果出现

> You must get working getaddrinfo() function. or you can specify "--disable-ipv6".

使用一下命令安装

```
PYTHON_CONFIGURE_OPTS="--disable-ipv6" proxychains  pyenv install 3.7.5

```



### Postman

#### 下载解压

[官网下载地址](https://learning.getpostman.com/docs/postman/launching_postman/installation_and_updates/#postman-native-apps)



#### 创建启动方式

创建一个桌面文件命名为：Postman.desktop。内容如下：

```
[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=/app/Postman/app/Postman %U
Icon=/app/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;

```

一旦创建Postman.desktop文件，就可以用应用程序启动器来打开，可以双击Postman图标打开。



### VMware

#### 下载

VMware不要从应用商店下载，直接去官网的[下载地址](https://my.vmware.com/zh/group/vmware/details?productId=362&downloadGroup=WKST-1006-LX)

VMware官网账号：

```
4670049@qq.com

1QAZ2wsx<>?
```



#### 授权安装

授权

```
chmod +x VMware-Workstation-Full-10.0.6-2700073.x86_64.bundle
```

安装

```
su sh VMware-Workstation-Full-10.0.6-2700073.x86_64.bundle
```

#### 激活码

CG54H-D8D0H-H8DHY-C6X7X-N2KG6

ZC3WK-AFXEK-488JP-A7MQX-XL8YF

AC5XK-0ZD4H-088HP-9NQZV-ZG2R4

ZC5XK-A6E0M-080XQ-04ZZG-YF08D

ZY5H0-D3Y8K-M89EZ-AYPEG-MYUA8

MA491-6NL5Q-AZAM0-ZH0N2-AAJ5A





### TensorFlow

```
python -m pip -install tensorflow==1.14.0 -i http://mirrors.aliyun.com/pypi/simple --trusted-host mirrors.aliyun.com
```

