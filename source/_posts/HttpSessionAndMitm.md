---
title:  HTTP Session与中间人攻击
date: 2019-05-14 17:22:40
updated: 2019-05-14 17:22:40
tags: [MITM,Session]
categories: HTTP
description: "上一篇写到了session丢失的问题，用户登录之后服务器存储用户信息在session中，然后给客户端或者浏览器返回一个sessionid,那么如果我连接了公共的wifi，被其他人抓包之后获取了http header中的sessionid，是不是就可以以用户的身份随意做一些权限操作呢？"
---
## 前言 ##
上一篇写到了Session丢失的问题，用户登录之后服务器存储用户信息在session中，然后给客户端或者浏览器返回一个sessionid,那么如果我连接了公共的wifi，被其他人抓包之后获取了http header中的sessionid，是不是就可以以用户的身份随意做一些权限操作呢？

答案是肯定的。只需要在headers中设置了你拿到的sessionid即可。

    //tomcat生成的sessionid叫做jsessionid。
    cookie=JSESSIONID=AC56B19FACB51BAE7843597AA9A53971


## session ##
### session的初衷 ###
session在web开发中是一个非常重要的概念，这个概念很抽象，很难定义，也是最让人迷惑的一个名词，也是最多被滥用的名字之一，在不同的场合，session一次的含义也很不相同。这里只探讨HTTP Session。

session的初衷是解决http协议无状态问题的服务端解决方案，它能让客户端和服务端一系列交互动作变成一个完整的事务，能使网站变成一个真正意义上的软件。

讲到这里就说一点题外话，在web应用开发里就出现了保持http链接状态的技术：一个是cookie技术，另一种是session技术。


> cookie技术是客户端的解决方案（当然随着html5的出现，比cookie更为强劲和安全的技术出现了，但是鉴于html5的普及度不够，就不做本文讨论的内容了），Cookie就是由服务器发给客户端的特殊信息，而这些信息以文本文件的方式存放在客户端，然后客户端每次向服务器发送请求的时候都会带上这些特殊的信息。让我们说得更具体一些：当用户使用浏览器访问一个支持Cookie的网站的时候，用户会提供包括用户名在内的个人信息并且提交至服务器；接着，服务器在向客户端回传相应的超文本的同时也会发回这些个人信息，当然这些信息并不是存放在HTTP响应体（Response > Body）中的，而是存放于HTTP响应头（Response > Header）；当客户端浏览器接收到来自服务器的响应之后，浏览器会将这些信息存放在一个统一的位置，对于Windows操作系统而言，我们可以从： [系统盘]:\Documents and > Settings\[用户名]\Cookies目录中找到存储的Cookie；自此，客户端再向服务器发送请求的时候，都会把相应的Cookie再次发回至服务器。而这次，Cookie信息则存放在HTTP请求头（Request > Header）了。有了Cookie这样的技术实现，服务器在接收到来自客户端浏览器的请求之后，就能够通过分析存放于请求头的Cookie得到客户端特有的信息，从而动态生成与该客户端相对应的内容。通常，我们可以从很多网站的登录界面中看到“请记住我”这样的选项，如果你勾选了它之后再登录，那么在下一次访问该网站的时候就不需要进行重复而繁琐的登录动作了，而这个功能就是通过Cookie实现的。
> 
> 
> session技术则是服务端的解决方案，它是通过服务器来保持状态的。由于Session这个词汇包含的语义很多，因此需要在这里明确一下Session的含义。首先，我们通常都会把Session翻译成会话，因此我们可以把客户端浏览器与服务器之间一系列交互的动作称为一个 Session。从这个语义出发，我们会提到Session持续的时间，会提到在Session过程中进行了什么操作等等；其次，Session指的是服务器端为客户端所开辟的存储空间，在其中保存的信息就是用于保持状态。从这个语义出发，我们则会提到往Session中存放什么内容，如何根据键值从 Session中获取匹配的内容等。要使用Session，第一步当然是创建Session了。那么Session在何时创建呢？当然还是在服务器端程序运行的过程中创建的，不同语言实现的应用程序有不同创建Session的方法，而在Java中是通过调用HttpServletRequest的getSession方法（使用true作为参数）创建的。在创建了Session的同时，服务器会为该Session生成唯一的Session id，而这个Session  id在随后的请求中会被用来重新获得已经创建的Session；在Session被创建之后，就可以调用Session相关的方法往Session中增加内容了，而这些内容只会保存在服务器中，发到客户端的只有Session id；当客户端再次发送请求的时候，会将这个Session id带上，服务器接受到请求之后就会依据Session id找到相应的Session，从而再次使用之。正式这样一个过程，用户的状态也就得以保持了。


**cookie与session的关系** 
cookie和session的方案虽然分别属于客户端和服务端，但是服务端的session的实现对客户端的cookie有依赖关系的，上面讲到服务端执行session机制时候会生成session的id值，这个id值会发送给客户端，客户端每次请求都会把这个id值放到http请求的头部发送给服务端，而这个id值在客户端会保存下来，保存的容器就是cookie，因此当我们完全禁掉浏览器的cookie的时候，服务端的session也会不能正常使用。

 ### Java中session的产生和存储 ###
session也被翻译成会话，sessionid是一个会话的key，浏览器第一次访问服务器会在服务器端生成一个session，有一个sessionid和它对应。

ManagerBase是所有session管理工具类的基类，它是一个抽象类，所有具体实现session管理功能的类都要继承这个类，该类有一个受保护的方法，该方法就是创建sessionId值的方法，（tomcat的session的id值生成的机制是一个随机数加时间加上jvm的id值，jvm的id值会根据服务器的硬件信息计算得来，因此不同jvm的id值都是唯一的）。

StandardManager类是tomcat容器里默认的session管理实现类，它会将session的信息存储到web容器所在服务器的内存里。

PersistentManagerBase也是继承ManagerBase类，它是所有持久化存储session信息的基类，PersistentManager继承了PersistentManagerBase，但是这个类只是多了一个静态变量和一个getName方法，目前看来意义不大，对于持久化存储session，tomcat还提供了StoreBase的抽象类，它是所有持久化存储session的基类，另外tomcat还给出了文件存储FileStore和数据存储JDBCStore两个实现。


**注意：** 为了弥补http协议的无状态的特点，服务端会占用一定的内存和cpu用来存储和处理session计算的开销，这也就是tomcat这个的web容器的并发连接那么低（tomcat官方文档里默认的连接数是200）原因之一。此处可以考虑使用分布式缓存技术，例如：memcached和redis，将session信息的存储独立出来也是解决session同步问题的方法。

Tomcat的session同步也有使用memcache的解决方案，大家可以参加下面的文章：
http://blog.sina.com.cn/s/blog_5376c71901017bqx.html
但是该方案只是解决了同步问题，session机制任然和web容器紧耦合，我们需要一个高效、可扩展的解决方案，那么我们就应该不是简单的把session独立出来存储而是设计一个完全独立的session机制，它既能给每个web应用提供session的功能又可以实现session同步，下面是一篇用zookeeper实现的分布式session方案：
http://www.open-open.com/lib/view/open1378556537303.html

## 中间人攻击 ##

中间人攻击（Man-in-the-MiddleAttack，简称“MITM攻击”）是一种“间接”的入侵攻击，这种攻击模式是通过各种技术手段将受入侵者控制的一台计算机虚拟放置在网络连接中的两台通信计算机之间，这台计算机就称为“中间人”。在前言中提到的就是简单的session劫持也叫中间人攻击。

中间人攻击主要有2种方式：
### **DNS欺骗（DNSSpoofing）** ###
> DNS欺骗就是其中的一种惯用手法。攻击者通过入侵DNS服务器、控制路由器等方法把受害者要访问的目标机器域名对应的IP解析为攻击者所控制的机器，这样受害者原本要发送给目标机器的数据就发到了攻击者的机器上，这时攻击者就可以监听甚至修改数据，从而收集到大量的信息。
> 
### **会话劫持（SessionHijack）** ###

>会话劫持是一种结合了嗅探以及欺骗技术在内的攻击手段。广义上说，会话劫持就是在一次正常的通信过程中，攻击者作为第三方参与到其中，或者是在数据里加入其他信息，甚至将双方的通信模式暗中改变，即从直接联系变成有攻击者参与的联系。简单地说，就是攻击者把自己插入到受害者和目标机器之间，并设法让受害者和目标机器之间的数据通道变为受害者和目标机器之间存在一个看起来像“中转站”的代理机器（攻击者的机器）的数据通道，从而干涉两台机器之间的数据传输，例如监听敏感数据、替换数据等。  
