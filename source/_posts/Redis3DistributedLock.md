---
title: 关于Redis(三)：分布式锁的应用
date: 2018-06-15 16:03:21
updated: 2018-06-15 16:03:21
tags: [Redis]
categories: NoSql
description: "因为平时工作中，线上服务器是分布式多台部署的，经常会遇到解决分布式场景下数据一致性的问题，这种情况就需要利用分布式锁解决这些问题。网上部分相关文章有误人子弟嫌疑，本文结合网上资料和个人实践得出，特此记录方便以后自己查阅，同时也希望能帮助他人。"
---
## 前言 ##
因为平时工作中，线上服务器是分布式多台部署的，经常会遇到解决分布式场景下数据一致性的问题，这种情况就需要利用分布式锁解决这些问题。网上部分相关文章有误人子弟嫌疑，本文结合网上资料和个人实践得出，特此记录方便以后自己查阅，同时也希望能帮助他人。

## 基本概念 ##
### 什么是锁 ###
锁就是让我们的系统有序的去对共享资源进行操作，通过互斥来保持数据的一致性。

在单机时代，虽然不需要分布式锁，但也面临过类似的问题，只不过在单机的情况下，如果有多个线程要同时访问某个共享资源的时候，我们可以采用线程间加锁的机制，即当某个线程获取到这个资源后，就立即对这个资源进行加锁，当使用完资源之后，再解锁，其它线程就可以接着使用了。例如，比如Java中的synchronize和Lock。

但是到了分布式系统的时代，这种线程之间的锁机制，就没作用了，系统可能会有多份并且部署在不同的机器上，这些资源已经不是在线程之间共享了，而是属于进程之间共享的资源。

因此，为了解决这个问题，我们就必须引入了分布式锁。

分布式锁，是指在分布式的部署环境下，通过锁机制来让多客户端互斥的对共享资源进行访问。

### 为什么需要分布式锁 ###
比如我们在网上抢商品时，点击购买后会先查询库存，库存大于1，才会下单，下单成功，库存减1。假设当前商品数为1，A进入之后查询数据库余量为1，因为网络或者业务原因，A的下单过程过慢，B进入时查询余量也为1，最后导致的结果就是A和B同时抢购成功，库存变为-1，此时就产生了脏数据，至于后果，不用多说，所以我们需要分布式锁来保证数据的一致性。

### 需要什么样的锁 ###

 1. 排他（互斥）性：在任意时刻，只有一个客户端能持有锁。
 2. 安全性：只有加锁的服务才能有解锁权限。
 3. 阻塞锁特性：即没有获取到锁，则继续等待获取锁。(根据业务需求)
 4. 非阻塞锁特性：即没有获取到锁，则直接返回获取锁失败。(根据业务需求)
 5. 锁失效机制：网络中断或宕机无法释放锁时，锁必须被删除，防止死锁。
 6. 可重入特性：一个线程中可以多次获取同一把锁，比如一个线程在执行一个带锁的方法，该方法中又调用了另一个需要相同锁的方法，则该线程可以直接执行调用的方法，而无需重新获得锁。

注意：不管加锁还是解锁都要保证原子性。

### 分布式锁有哪些实现方法 ###
目前主流的有三种，从实现的复杂度上来看，从上往下难度依次增加：
 1. 基于数据库实现（乐观锁 、悲观锁）
 2. 基于缓存实现（本文只讲非集群Redis）
 3. 基于ZooKeeper实现

## 分布式锁实现 ##
### 方法一：数据库（乐观锁） ###
这个策略源于 mysql 的 mvcc 机制，使用这个策略其实本身没有什么问题，唯一的问题就是对数据表侵入较大，我们要为每个表设计一个版本号字段，然后写一条判断 sql 每次进行判断，增加了数据库操作的次数，在高并发的要求下，对数据库连接的开销也是无法忍受的。
> 乐观锁通常实现基于数据版本(version)的记录机制实现的，比如有一张红包表（t_bonus），有一个字段(left_count)记录礼物的剩余个数，用户每领取一个奖品，对应的left_count减1，在并发的情况下如何要保证left_count不为负数，乐观锁的实现方式为在红包表上添加一个版本号字段（version），默认为0。

异常实现流程

    -- 可能会发生的异常情况
    -- 线程1查询，当前left_count为1，则有记录
    select * from t_bonus where id = 10001 and left_count > 0
     
    -- 线程2查询，当前left_count为1，也有记录
    select * from t_bonus where id = 10001 and left_count > 0
     
    -- 线程1完成领取记录，修改left_count为0,
    update t_bonus set left_count = left_count - 1 where id = 10001
     
    -- 线程2完成领取记录，修改left_count为-1，产生脏数据
    update t_bonus set left_count = left_count - 1 where id = 10001

通过乐观锁实现

    -- 添加版本号控制字段
    ALTER TABLE table ADD COLUMN version INT DEFAULT '0' NOT NULL AFTER t_bonus;
     
    -- 线程1查询，当前left_count为1，则有记录，当前版本号为1234
    select left_count, version from t_bonus where id = 10001 and left_count > 0
     
    -- 线程2查询，当前left_count为1，有记录，当前版本号为1234
    select left_count, version from t_bonus where id = 10001 and left_count > 0
     
    -- 线程1,更新完成后当前的version为1235，update状态为1，更新成功
    update t_bonus set version = 1235, left_count = left_count-1 where id = 10001 and version = 1234
     
    -- 线程2,更新由于当前的version为1235，udpate状态为0，更新失败，再针对相关业务做异常处理
    update t_bonus set version = 1235, left_count = left_count-1 where id = 10001 and version = 1234


### 方法二：基于缓存（Redis）###
依赖引入，直接用redis官网原生。

    <dependency>
        <groupId>redis.clients</groupId>
        <artifactId>jedis</artifactId>
        <version>2.9.0</version>
    </dependency>

#### 加锁 ####

    @Resource
        private JedisPool jedisPool;
    
        private static final String LOCK_SUCCESS;
        private static final String SET_IF_NOT_EXIST;
        private static final String SET_WITH_EXPIRE_TIME;
    
        private static final Long RELEASE_SUCCESS = 1L;
    
        public static final String UNLOCK_LUA;
    
        static {
            StringBuilder sb = new StringBuilder();
            sb.append("if redis.call(\"get\",KEYS[1]) == ARGV[1] ");
            sb.append("then ");
            sb.append("    return redis.call(\"del\",KEYS[1]) ");
            sb.append("else ");
            sb.append("    return 0 ");
            sb.append("end ");
            UNLOCK_LUA = sb.toString();
            LOCK_SUCCESS = "OK";
            SET_IF_NOT_EXIST = "NX";
            SET_WITH_EXPIRE_TIME = "PX";
        }

尝试获取分布式锁


       /**
         * 尝试获取分布式锁
         *
         * @param lockKey    锁
         * @param requestId  请求标识
         * @param expireTime 超期时间
         * @return 是否获取成功
         */
        public boolean tryGetLock(String lockKey, String requestId, long expireTime) {
            Jedis jedis = getJedis();
            try {
                String result = jedis.set(lockKey, requestId, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);
                if (LOCK_SUCCESS.equals(result)) {
                    return true;
                }
                return false;
            } catch (Exception e) {
                logger.error("set redis occured an exception", e);
            } finally {
                returnJedis(jedis);
            }
            return false;
        }
上述代码可以看出我们加锁就一行代码：`jedis.set(String key, String value, String nxxx, String expx, int time)`，这个set()方法一共有五个形参：

 1. 第一个为key，我们使用key来当锁，因为key是唯一的。
 2. 第二个为value，我们传的是requestId，有人可能有疑问，有key作为锁不就够了吗，为什么还要用到value？原因就是我们在上面讲到安全性时，通过给value赋值为requestId，我们就知道这把锁是哪个请求加的了，在解锁的时候就可以有依据。requestId可以使用UUID.randomUUID().toString()方法生成。
 3. 第三个为nxxx，这个参数我们填的是NX，意思是SET IF NOT  EXIST，即当key不存在时，我们进行set操作；若key已经存在，则不做任何操作；
 4. 第四个为expx，这个参数我们传的是PX，意思是我们要给这个key加一个过期的设置，具体时间由第五个参数决定。
 5. 第五个为time，与第四个参数相呼应，代表key的过期时间。

总的来说，执行上面的set()方法就只会导致两种结果：1. 当前没有锁（key不存在），那么就进行加锁操作，并对锁设置个有效期，同时value表示加锁的客户端。2. 已有锁存在，不做任何操作。

首先，set()加入了NX参数，可以保证如果已有key存在，则函数不会调用成功，也就是只有一个客户端能持有锁，满足互斥性。
其次，由于我们对锁设置了过期时间，即使锁的持有者后续发生崩溃而没有解锁，锁也会因为到了过期时间而自动解锁（即key被删除），不会发生死锁，也满足了锁失效机制。
最后，因为我们将value赋值为requestId，代表加锁的客户端请求标识，那么在客户端在解锁的时候就可以进行校验是否是同一个客户端。
**但是这里还是会有一个问题就是，网上很多文章都没有讲到，如果某些原因导致线程A执行的很慢很慢，过了30秒（过期时间）都没执行完，这时候锁过期自动释放，线程B得到了锁。怎么办呢？我们可以让获得锁的线程开启一个守护线程，用来给快要过期的锁“续航”，当过去了29秒，线程A还没执行完，这时候守护线程会执行expire指令，为这把锁“续命”20秒。守护线程从第29秒开始执行，每20秒执行一次。如果节点1 忽然断电，由于线程A和守护线程在同一个进程，守护线程也会停下。这把锁到了超时的时候，没人给它续命，也就自动释放了。**

由于我们只考虑Redis单机部署的场景，所以容错性我们暂不考虑。


错误示例1
比较常见的错误示例就是使用jedis.setnx()和jedis.expire()组合实现加锁，代码如下：


    public static void wrongGetLock1(Jedis jedis, String lockKey, String requestId, int expireTime) {
    
        Long result = jedis.setnx(lockKey, requestId);
        if (result == 1) {
            // 若在这里程序突然崩溃，则无法设置过期时间，将发生死锁
            jedis.expire(lockKey, expireTime);
        }
    
    }

setnx()方法作用就是SET IF NOT EXIST，expire()方法就是给锁加一个过期时间。乍一看好像和前面的set()方法结果一样，然而由于这是两条Redis命令，不具有原子性，如果程序在执行完setnx()之后突然崩溃，导致锁没有设置过期时间。那么将会发生死锁。网上之所以有人这样实现，是因为低版本的jedis并不支持多参数的set()方法。

错误示例2

    public static boolean wrongGetLock2(Jedis jedis, String lockKey, int expireTime) {
    
        long expires = System.currentTimeMillis() + expireTime;
        String expiresStr = String.valueOf(expires);
    
        // 如果当前锁不存在，返回加锁成功
        if (jedis.setnx(lockKey, expiresStr) == 1) {
            return true;
        }
    
        // 如果锁存在，获取锁的过期时间
        String currentValueStr = jedis.get(lockKey);
        if (currentValueStr != null && Long.parseLong(currentValueStr) < System.currentTimeMillis()) {
            // 锁已过期，获取上一个锁的过期时间，并设置现在锁的过期时间
            String oldValueStr = jedis.getSet(lockKey, expiresStr);
            if (oldValueStr != null && oldValueStr.equals(currentValueStr)) {
                // 考虑多线程并发的情况，只有一个线程的设置值和当前值相同，它才有权利加锁
                return true;
            }
        }
            
        // 其他情况，一律返回加锁失败
        return false;
    
    }

这一种错误示例就比较难以发现问题，而且实现也比较复杂。实现思路：使用jedis.setnx()命令实现加锁，其中key是锁，value是锁的过期时间。执行过程：1. 通过setnx()方法尝试加锁，如果当前锁不存在，返回加锁成功。2. 如果锁已经存在则获取锁的过期时间，和当前时间比较，如果锁已经过期，则设置新的过期时间，返回加锁成功。


那么这段代码问题在哪里？1. 由于是客户端自己生成过期时间，所以需要强制要求分布式下每个客户端的时间必须同步。 2. 当锁过期的时候，如果多个客户端同时执行jedis.getSet()方法，那么虽然最终只有一个客户端可以加锁，但是这个客户端的锁的过期时间可能被其他客户端覆盖。3. 锁不具备拥有者标识，即任何客户端都可以解锁。

#### 解锁 ####

    /**
         * 释放分布式锁
         *
         * @param lockKey   锁
         * @param requestId 请求标识
         * @return 是否释放成功
         */
        public boolean releaseLock(String lockKey, String requestId) {
            // 释放锁的时候，有可能因为持锁之后方法执行时间大于锁的有效期，此时有可能已经被另外一个线程持有锁，所以不能直接删除
            Jedis jedis = getJedis();
            try {
                List<String> keys = Collections.singletonList(lockKey);
                List<String> args = Collections.singletonList(requestId);
                // 使用lua脚本删除redis中匹配value的key，可以避免由于方法执行时间过长而redis锁自动过期失效的时候误删其他线程的锁
                // spring自带的执行脚本方法中，集群模式直接抛出不支持执行脚本的异常，所以只能拿到原redis的connection来执行脚本
                Long result = (Long) jedis.eval(UNLOCK_LUA, keys, args);
                return RELEASE_SUCCESS.equals(result);
            } catch (Exception e) {
                logger.error("release lock occured an exception", e);
            } finally {
                returnJedis(jedis);
            }
            return false;
        }


可以看到，我们解锁只需要两行代码就搞定了！第一行代码，我们写了一个简单的Lua脚本代码，上一次见到这个编程语言还是在《黑客与画家》里，没想到这次居然用上了。第二行代码，我们将Lua代码传到jedis.eval()方法里，并使参数KEYS[1]赋值为lockKey，ARGV[1]赋值为requestId。eval()方法是将Lua代码交给Redis服务端执行。

那么这段Lua代码的功能是什么呢？其实很简单，首先获取锁对应的value值，检查是否与requestId相等，如果相等则删除锁（解锁）。那么为什么要使用Lua语言来实现呢？因为要确保上述操作是原子性的。关于非原子性会带来什么问题，可以阅读【解锁代码-错误示例2】 。那么为什么执行eval()方法可以确保原子性，源于Redis的特性，下面是官网对eval命令的部分解释：



> 简单来说，就是在eval命令执行Lua代码的时候，Lua代码将被当成一个命令去执行，并且直到eval命令执行完成，Redis才会执行其他命令。

错误示例1
最常见的解锁代码就是直接使用jedis.del()方法删除锁，这种不先判断锁的拥有者而直接解锁的方式，会导致任何客户端都可以随时进行解锁，即使这把锁不是它的。

    public static void wrongReleaseLock1(Jedis jedis, String lockKey) {
        jedis.del(lockKey);
    }

 


错误示例2
这种解锁代码乍一看也是没问题，甚至我之前也差点这样实现，与正确姿势差不多，唯一区别的是分成两条命令去执行，代码如下：



    public static void wrongReleaseLock2(Jedis jedis, String lockKey, String requestId) {
            
        // 判断加锁与解锁是不是同一个客户端
        if (requestId.equals(jedis.get(lockKey))) {
            // 若在此时，这把锁突然不是这个客户端的，则会误解锁
            jedis.del(lockKey);
        }
    }

如代码注释，问题在于如果调用jedis.del()方法的时候，这把锁已经不属于当前客户端的时候会解除他人加的锁。那么是否真的有这种场景？答案是肯定的，比如客户端A加锁，一段时间之后客户端A解锁，在执行jedis.del()之前，锁突然过期了，此时客户端B尝试加锁成功，然后客户端A再执行del()方法，则将客户端B的锁给解除了。

### 方法三：ZooKeeper
后面有时间补充吧，太懒了。
## 总结 ##
本文**主要**介绍了如何使用Java代码正确实现Redis分布式锁，对于加锁和解锁也分别给出了两个比较经典的错误示例。被这两个错误示例坑惨了（小声哔哔），互联网虽然给我们带来了方便，但也不能失去应有的质疑精神，多想多验证。

如果你的项目中Redis是多机部署的，那么可以尝试使用Redisson实现分布式锁，这是Redis官方提供的Java组件。