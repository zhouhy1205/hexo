---
title: Hexo系列（六）：添加评论功能
date: 2017-05-12 12:44:27
updated: 2017-05-12 12:44:27
tags: [Comment]
categories: Hexo
description: "博客评论功能的重要性不言而喻，拥有了评论功能就相当于拥有了一批志同道合的良师益友，可以指出文章中的错误得到提高，可以讨论加深理解与认同。"
---
## 前言 ##
> 博客评论功能的重要性不言而喻，拥有了评论功能就相当于拥有了一批志同道合的良师益友，可以指出文章中的错误得到提高，可以讨论加深理解与认同。

## 介绍 ##
`yelee`主题目前支持 Disqus，多说 及 友言评论，把 `#on: true` 改为 `on: true` 即启用对应评论系统
但是多说已经关闭，友言是基于Web的，Disqus在手机上展示不出来，发现还需要翻墙，那实在对用户太不友好了，于是我选择使用[来必力](https://www.livere.com/)评论系统。
![来必力官网首页](Hexo6AddCommentFunction/来必力官网首页.jpg)

### 注册 ###
注册后可以查看详细的评论数据分析。
![数据分析](Hexo6AddCommentFunction/数据分析.jpg)
### 获取data-uid ###
代码管理，我们需要用到这个`data-uid`
![代码](Hexo6AddCommentFunction/代码.jpg)

### 添加data-uid ###
打开`theme/yelee/_config.yml`，添加配置信息

    livere:
        on: true
        livere_uid: Your uid
### 创建ejs文件 ###
在`themes/yelee/layout/_partial/comments`文件夹创建`livere.ejs`文件，将代码拷贝进去，将你注册后的代码拷贝到`<section></section`>里面。

    <section class="livere" id="comments">
        <!-- 来必力City版安装代码 -->
        <div id="lv-container" data-id="city" data-uid="Your uid">
        <script type="text/javascript">
       (function(d, s) {
           var j, e = d.getElementsByTagName(s)[0];
    
           if (typeof LivereTower === 'function') { return; }
    
           j = d.createElement(s);
           j.src = 'https://cdn-city.livere.com/js/embed.dist.js';
           j.async = true;
    
           e.parentNode.insertBefore(j, e);
           })(document, 'script');
        </script>
        <noscript> 为正常使用来必力评论功能请激活JavaScript</noscript>
        </div>
        <!-- City版安装代码已完成 -->
    </section>


### 追加逻辑判断 ###
打开`themes/yelee/layout/_partial/article.ejs`，在下图位置插入下面的逻辑判断代码

    else if (theme.livere.on) { %>
        <%- partial('comments/livere') %>
    <% } 
![插入位置](Hexo6AddCommentFunction/插入位置.jpg)

到这一步就完成了，集成评论。
![评论](Hexo6AddCommentFunction/评论.jpg)