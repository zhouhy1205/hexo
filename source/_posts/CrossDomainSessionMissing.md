---
title:  关于跨域问题中Session会话丢失问题
date: 2017-12-13 13:58:42
updated: 2017-12-13 13:58:42
tags: [Session]
categories: CrossDomain
description: "目前公司开的项目使用前后端分离的模式开发，后端提供跨域接口、前端json调用，绑定数据，以此解决跨域问题。目前使用前后端分离的模式开发，后端提供跨域接口、前端json调用，绑定数据，以此解决跨域问题。而Tomcat使用cookie中jsessionid来区分客户端session会话，跨域请求接口恰恰有时候响应回来会改变该站点下的jsessionid值，导致服务器每次判断都是一个新的会话。"
---

## 原因 ##
目前公司开的项目使用前后端分离的模式开发，后端提供跨域接口、前端json调用，绑定数据，以此解决跨域问题。

而Tomcat使用cookie中jsessionid来区分客户端session会话

跨域请求接口恰恰有时候响应回来会改变该站点下的jsessionid值，导致服务器每次判断都是一个新的会话。

所以就造成了后台每次接受到的都是一个新的Session,也就是标题说的Session丢失的问题。

可以通过后台打印jsessionid或者网站F12看set-cookie直观的看出问题，这里就补贴图了。

## 场景 ##
一般在网站登录模块，用户登录后，服务器会在Session中设置相应的属性来记录用户的登录信息（比如此时的jsessionid是A），避免其他的接口重复登录。

    //设置
    request.getSession().setAttribute("user", stageUser);

当登录成功后调用下一个业务接口，会在过滤器或者拦截器从Session中取出用户的登录信息，进行跨域时，每一次请求都没有将sessinId带过去，服务器就认为是一个没有登录的新请求，都会在cookie里set一个新的sessionId，所以之前存的值永远也取不到,这就是所谓的Session丢失。

    //获取登录信息
    StageUser user = (StageUser) request.getSession().getAttribute("user");


## 解决 ##
使用的标准是CORS，即跨域资源共享。CORS是一个W3C标准，全称是”跨域资源共享”(Cross-origin resource sharing)。

它允许浏览器向跨源服务器，发出XMLHttpRequest请求，从而克服了只能同源使用的限制。

ajax请求设置为如下：

    $.ajax({
    		type:'POST',
    		url:url,
    		async:isAsynchronous,
    		data:param,
    		dataType:'json',
    		timeout:outTime,      // 超时45秒
    		contentType: "application/json",
    		//加上 xhrFields及crossDomain
            xhrFields: { 
                withCredentials: true//允许带上凭据
            },
            crossDomain: true,
    		success:function(data){
    		}
    });

默认情况下，跨源请求不提供凭据(cookie、HTTP认证及客户端SSL证明等)。通过将withCredentials属性设置为true，可以指定某个请求应该发送凭据。如果服务器接收带凭据的请求，会用下面的HTTP头部来响应。

虽然设置了widthCredentials为true的请求中会包含远程域的所有cookie，但这些cookie仍然遵循同源策略，所以外域是访问不了这些cookie的，现在我们就可以安全地跨域访问啦。


后台统一添加

    response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));//处理跨域
    response.setHeader("Access-Control-Allow-Credentials", "true");//表示是否允许发送Cookie


在一开始解决跨域时，我采用的是 response.setHeader("Access-Control-Allow-Origin", "*") 处理跨域，表示接受任意域名的请求。但是设置了response.setHeader("Access-Control-Allow-Credentials", "true")后，就不能使用“*”，不过要处理好跨域，可以直接设置允许请求的域名：response.setHeader("Access-Control-Allow-Origin", "域名名称")，我在这里直接取出当前域名，又设置允许，同“*”作用相同。