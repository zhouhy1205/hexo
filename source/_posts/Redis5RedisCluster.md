---
title: 关于Redis(五)：Cluster模式
date: 2020-05-21 16:03:21
updated: 2020-05-21 16:03:21
tags: [Redis]
categories: NoSql
description: "Redis集群讲了主从模式和哨兵模式，就差一个Redis Cluster了,Redis Cluster模式综合了主从和哨兵的优点，也是互联网公司用的比较多的模式，搭建也相对比较简单。"
---

## 前言 ##

Redis集群的三种模式主从模式、Sentinel模式、Cluster模式，就差一个Cluster了,Cluster模式综合了主从和哨兵的优点，也是互联网公司用的比较多的模式，搭建也相对比较简单。

Cluster模式的高可靠性是三种模式中最高的，达到了99.99%，也就是4个9。

> 在软件系统的高可靠性(也称为可用性，英文描述为HA，High Available)里有个衡量其可靠性的标准——X个9，这个X是代表数字3~5。X个9表示在软件系统1年时间的使用过程中，系统可以正常使用时间与总时间(1年)之比，我们通过下面的计算来感受下X个9在不同级别的可靠性差异。
>
> - 3个9：(1-99.9%)36524=8.76小时，表示该软件系统在连续运行1年时间里最多可能的业务中断时间是8.76小时。
> - 4个9：(1-99.99%)36524=0.876小时=52.6分钟，表示该软件系统在连续运行1年时间里最多可能的业务中断时间是52.6分钟。
> - 5个9：(1-99.999%)3652460=5.26分钟，表示该软件系统在连续运行1年时间里最多可能的业务中断时间是5.26分钟。
> - 1个9：(1-90%)365=36.5天
> - 2个9：(1-99%)365=3.65天
> - 6个9：(1-99.9999%)3652460*60=31秒

## 正文 ##

### **模式介绍**

sentinel模式基本可以满足一般生产的需求，具备高可用性。但是当数据量过大到一台服务器存放不下的情况时，主从模式或sentinel模式就不能满足需求了，这个时候需要对存储的数据进行分片，将数据存储到多个Redis实例中。cluster模式的出现就是为了解决单机Redis容量有限的问题，将Redis的数据根据一定的规则分配到多台机器。

cluster可以说是sentinel和主从模式的结合体，通过cluster可以实现主从和master重选功能，所以如果配置两个副本三个分片的话，就需要六个Redis实例。因为Redis的数据是根据一定规则分配到cluster的不同机器的，当数据量过大时，可以新增机器进行扩容。

使用集群，只需要将redis配置文件中的`cluster-enable`配置打开即可。每个集群中至少需要三个主数据库才能正常运行，新增节点非常方便。

![集群模式](Redis5RedisCluster/cluster.png)

Cluster整个集群有16384个槽位，16384个槽位分发整个集群的所有主节点（分配可自定义），从节点仅同步主节点数据。当set键值的时候对key进行哈希计算得出hash value,hash value对16384取余，得出的值为多少，就分配到哪个槽位存储。



### 扩展内容

为什么是16384？

> **1、**正常的心跳包携带节点的完整配置，可以用幂等方式替换旧节点以更新旧配置。 这意味着它们包含原始形式的节点的插槽配置，它使用带有16k插槽的2k空间，但使用65k插槽时将使用高达8k的空间。
> **2、**同时，由于其他设计权衡，Redis Cluster不太可能扩展到超过1000个主节点。
> 因此，16k处于正确的范围内，以确保每个主站有足够的插槽，最多1000个主站，但足够小的数字可以轻松地将插槽配置传播为原始位图。 请注意，在小型集群中，位图难以压缩，因为当N很小时，位图将设置插槽/ N位，这是设置的大部分位。
>
> 
>
> **其它：**在redis节点发送心跳包时需要把所有的槽放到这个心跳包里，以便让节点知道当前集群信息，16384=16k，在发送心跳包时使用char进行bitmap压缩后是2k（2 * 8 (8 bit) * 1024(1k) = 2K），也就是说使用2k的空间创建了16k的槽数。虽然使用CRC16算法最多可以分配65535（2^16-1）个槽位，65535=65k，压缩后就是8k（8 * 8 (8 bit) * 1024(1k) = 8K），也就是说需要需要8k的心跳包，作者认为这样做不太值得；并且一般情况下一个redis集群不会有超过1000个master节点，所以16k的槽位是个比较合适的选择.
>
> **(1)如果槽位为65536，发送心跳信息的消息头达8k，发送的心跳包过于庞大。**
> 如上所述，在消息头中，最占空间的是`myslots[CLUSTER_SLOTS/8]`。
> 当槽位为65536时，这块的大小是:`65536÷8÷1024=8kb`
> 因为每秒钟，redis节点需要发送一定数量的ping消息作为心跳包，如果槽位为65536，这个ping消息的消息头太大了，浪费带宽。
> **(2)redis的集群主节点数量基本不可能超过1000个。**
> 如上所述，集群节点越多，心跳包的消息体内携带的数据越多。如果节点过1000个，也会导致网络拥堵。因此redis作者，不建议redis cluster节点数量超过1000个。
> 那么，对于节点数在1000以内的redis cluster集群，16384个槽位够用了。没有必要拓展到65536个。
> **(3)槽位越小，节点少的情况下，压缩率高**
> Redis主节点的配置信息中，它所负责的哈希槽是通过一张bitmap的形式来保存的，在传输过程中，会对bitmap进行压缩，但是如果bitmap的填充率slots / N很高的话(N表示节点数)，bitmap的压缩率就很低。
> 如果节点数很少，而哈希槽数量很多的话，bitmap的压缩率就很低。



### **特点**

```shell
#多个redis节点网络互联，数据共享

#所有的节点都是一主一从（也可以是一主多从），其中从不提供服务，仅作为备用 

#客户端可以连接任何一个主节点进行读写

#不支持同时处理多个key（如MSET/MGET），因为redis需要把key均匀分布在各个节点上，并发量很高的情况下同时创建key-value会降低性能并导致不可预测的行为   * 支持在线增加、删除节点。
```

### **部署**

#### **环境准备**

```shell
#没有那么多机器所以在一个机器上部署的。
#填写内网ip，如果在不同的内网可以使用公网ip
#192.168.1.101 端口7001,7002,7003,7004,7005,7006
```

#### **配置文件**

```shell
mkdir /usr/local/redis/cluster
cp /usr/local/redis/redis.conf /usr/local/redis/cluster/redis_7001.conf
chown -R redis:redis /usr/local/redis*
```

redis_7001.conf 配置修改

```shell
vim /usr/local/redis/cluster/redis_7001.conf
bind 192.168.1.101 
port 7001 
#启用守护线程，即后台运行
daemonize yes 
pidfile "/var/run/redis_7001.pid" 
logfile "/usr/local/redis/cluster/redis_7001.log" 
dir "/data/redis/cluster/redis_7001"
#replicaof 192.168.1.101 6379
#主节点密码
masterauth 123456
#当前节点密码
requirepass 123456 
appendonly yes 
cluster-enabled yes 
cluster-config-file nodes_7001.conf 
cluster-node-timeout 15000
```

将redis_7001.conf的配置中7001替换为7002,7003,7004,7005,7006并放在相应的目录下，我这里是6个redis密码都是123456,如果不相同也需要修改。

```shell
sed '/s/7001/7002/g' /usr/local/redis/cluster/redis_7001.conf > /usr/local/redis/cluster/redis_7002.conf

#启动6个服务
redis-server /usr/local/redis/cluster/redis_7001.conf
tail -f /usr/local/redis/cluster/redis_7001.log
```



#### 关联节点、分配槽位

Redis5.x以前需要安装ruby，使用**redis-trib**进行操作，过于繁琐且不常用这里不讲了。此处使用**redis-cli** 命令

```shell
#查看redis-cli下有哪些命令
#redis-cli --cluster help
root@caaee4f5f282:/# redis-cli --cluster help
Cluster Manager Commands:
#关联节点、分配槽位
  create         host1:port1 ... hostN:portN
                 --cluster-replicas <arg>
  check          host:port
                 --cluster-search-multiple-owners
  info           host:port
  fix            host:port
                 --cluster-search-multiple-owners
                 --cluster-fix-with-unreachable-masters
#从新分片
  reshard        host:port
                 --cluster-from <arg>
                 --cluster-to <arg>
                 --cluster-slots <arg>
                 --cluster-yes
                 --cluster-timeout <arg>
                 --cluster-pipeline <arg>
                 --cluster-replace
  rebalance      host:port
                 --cluster-weight <node1=w1...nodeN=wN>
                 --cluster-use-empty-masters
                 --cluster-timeout <arg>
                 --cluster-simulate
                 --cluster-pipeline <arg>
                 --cluster-threshold <arg>
                 --cluster-replace
#添加节点
  add-node       new_host:new_port existing_host:existing_port
                 --cluster-slave
                 --cluster-master-id <arg>
#删除节点
  del-node       host:port node_id
  call           host:port command arg arg .. arg
  set-timeout    host:port milliseconds
  import         host:port
                 --cluster-from <arg>
                 --cluster-copy
                 --cluster-replace
  backup         host:port backup_directory
  help

For check, fix, reshard, del-node, set-timeout you can specify the host and port of any working node in the cluster.
```

**关联节点、分配槽位**

```shell
#这里我们要使用的命令是create，关联和分配槽位一条命令搞定
redis-cli --cluster create 192.168.1.101:7001 192.168.1.101:7002 192.168.1.101:7003 192.168.1.101:7004 192.168.1.101:7005 192.168.1.101:7006 --cluster-replicas 1
#此处的--cluster-replicas 1 中的是从：主为1：1,前三台为主，后三台为从，主节点至少为3个。
```

进入任意节点查看节点信息

```shell
192.168.1.101:7001> cluster nodes
#c29f322cf58a7ff10a1357995e5ce0792da5b2c2 节点id
#10923-16383 槽位
c29f322cf58a7ff10a1357995e5ce0792da5b2c2 192.168.1.101:7003@17003 master - 0 1599318471486 3 connected 10923-16383
8bb12c309b58fc15bd524f349c5fe5a50ed6faf2 192.168.1.101:7002@17002 master - 0 1599318470483 2 connected 5461-10922
#d93f423156f246ff12ecf02e1b7d2de16b840b6b 主节点id d93f423156f246ff12ecf02e1b7d2de16b840b6b
05698adaeffff10d6dfeb6e7a871a4dc33c41de6 192.168.1.101:7005@17005 slave d93f423156f246ff12ecf02e1b7d2de16b840b6b 0 1599318470000 1 connected
1fefe4e0cdbbab9450584e7572af7b7d17b7be3b 192.168.1.101:7006@17006 slave 8bb12c309b58fc15bd524f349c5fe5a50ed6faf2 0 1599318469000 2 connected
e3ff11658e1e2189504458f7be5464bf32fa113b 192.168.1.101:7004@17004 slave c29f322cf58a7ff10a1357995e5ce0792da5b2c2 0 1599318469000 3 connected
d93f423156f246ff12ecf02e1b7d2de16b840b6b 192.168.1.101:7001@17001 myself,master - 0 1599318467000 1 connected 0-5460
192.168.1.101:7001>
#可查看集群信息，太长了，不贴了。
192.168.1.101:7001> CLUSTER INFO
#更改对应节点身份,将某个节点设置为master节点
192.168.1.101:7008> CLUSTER REPLICATE <节点id>
```

#### 集群扩容

```shell
#新增redis服务并启动，配置同上。
192.168.1.101 端口7007,7008
```

#### **添加节点**

```shell
#添加为主节点
redis-cli --cluster add-node 192.168.1.101:7007 <redis集群中任意IP>:<IP对应的端口号>
#添加为刚才新建主节点(192.168.1.101:7007)的从节点
redis-cli --cluster add-node 192.168.1.101:7008 <redis集群中任意IP>:<IP对应的端口号> --cluster-master-id <192.168.1.101:7007的节点id>
#此时新增的节点并没有分配槽位需要使用reshard命令
redis-cli --cluster reshard <集群中任意ip>:<IP对应的端口号>
#此处会提示需要分配多少个槽位，此处分配1000
How many slots do you want to move(form 1 to 16384)? 1000
#此处会提示分配给哪个节点
What is the receiving node ID? <192.168.1.101:7007的节点ID>
#此处提示从从那个主节点挤出槽位给新增主节点。如果是从所有主节点平均分配输入all并回车，即7001,7002,7003节点的槽位分别减少333,333,334个。
#以下命令是从7001节点,7002节点各分500个槽位给新增主节点
Source node #1:<7001节点的ID>
Source node #2:<7002节点的ID>
Source node #3:done
```

删除节点同理。



## **总结**

Redis相关的东西再来一篇集群在SpinngBoot下的应用以及关键知识点的总结就差不多了。