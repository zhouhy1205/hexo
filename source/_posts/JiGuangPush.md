---
title: 极光推送功能集成
date: 2018-12-15 16:44:21
updated: 2018-12-15 16:44:21
tags: [JiGuang,Push]
categories: Message 
description: "现在手机App中，每天都能收到大量的推送，这种推送是让用户了解app最新活动与消息，也是提醒用户参与活动并使用app的一个方法，在app中是必不可少的"
---
## 前言 ##
最近一直想写点东西，但是沉迷吃鸡无法自拔。还有就是我比较懒，稍有少许安逸就不想行动了。
今天主要是集成极光服务的推送功能，直接记下来好了。我的Java项目主要是需要集成极光的推送消息给android、IOS，所以在这里我只会介绍怎么给极光推送，至于APP端也很简单，根据你的推送方式设置别名，标签或者ID即可。
## 正文 ##
### 注册极光 ###
第一步，注册极光账号，这个就不需要多说了，[极光官网](https://www.jiguang.cn)。
注册好了之后创建一个应用，创建完成后,记住AppKey 与 Master Secret 后面会用到。

### 引入依赖 ###

	<dependency>
		<groupId>cn.jpush.api</groupId>
		<artifactId>jpush-client</artifactId>
		<version>3.2.17</version>
	</dependency>


现在一般都是Maven项目吧！！！如果不是去[Maven仓库](https://mvnrepository.com)下载。

### 创建Jpush帮助类 ###


    private final static String appKey = "860d817c725842c111111111";
    private final static String masterSecret = "36169d64e73ccd3111111111";
    private static JPushClient jPushClient = new JPushClient(masterSecret, appKey);

AppKey 和  Master Secret 就是前面创建应用得到的。
JPushClient 就是我们创建的实例
以下是具体推送的代码

    /**
         * 推送给设备标识参数的用户
         *
         * @param registrationId     设备标识
         * @param notification_title 通知内容标题
         * @param msg_title          消息内容标题
         * @param msg_content        消息内容
         * @param extrasparam        扩展字段
         * @return 0推送失败，1推送成功
         */
        public static int sendToRegistrationId(String registrationId, String notification_title, String msg_title, String msg_content, String extrasparam) {
            int result = 0;
            try {
                PushPayload pushPayload = JpushClientUtil.buildPushObject_all_registrationId_alertWithTitle(registrationId, notification_title, msg_title, msg_content, extrasparam);
                System.out.println(pushPayload);
                PushResult pushResult = jPushClient.sendPush(pushPayload);
                System.out.println(pushResult);
                if (pushResult.getResponseCode() == 200) {
                    result = 1;
                }
            } catch (APIConnectionException e) {
                e.printStackTrace();
    
            } catch (APIRequestException e) {
                e.printStackTrace();
            }
    
            return result;
        }
    
        /**
         * 发送给所有安卓用户
         *
         * @param notification_title 通知内容标题
         * @param msg_title          消息内容标题
         * @param msg_content        消息内容
         * @param extrasparam        扩展字段
         * @return 0推送失败，1推送成功
         */
        public static int sendToAllAndroid(String notification_title, String msg_title, String msg_content, String extrasparam) {
            int result = 0;
            try {
                PushPayload pushPayload = JpushClientUtil.buildPushObject_android_all_alertWithTitle(notification_title, msg_title, msg_content, extrasparam);
                System.out.println(pushPayload);
                PushResult pushResult = jPushClient.sendPush(pushPayload);
                System.out.println(pushResult);
                if (pushResult.getResponseCode() == 200) {
                    result = 1;
                }
            } catch (Exception e) {
    
                e.printStackTrace();
            }
    
            return result;
        }
    
        /**
         * 发送给所有IOS用户
         *
         * @param notification_title 通知内容标题
         * @param msg_title          消息内容标题
         * @param msg_content        消息内容
         * @param extrasparam        扩展字段
         * @return 0推送失败，1推送成功
         */
        public static int sendToAllIos(String notification_title, String msg_title, String msg_content, String extrasparam) {
            int result = 0;
            try {
                PushPayload pushPayload = JpushClientUtil.buildPushObject_ios_all_alertWithTitle(notification_title, msg_title, msg_content, extrasparam);
                System.out.println(pushPayload);
                PushResult pushResult = jPushClient.sendPush(pushPayload);
                System.out.println(pushResult);
                if (pushResult.getResponseCode() == 200) {
                    result = 1;
                }
            } catch (Exception e) {
    
                e.printStackTrace();
            }
    
            return result;
        }
    
        /**
         * 发送给所有用户
         *
         * @param notification_title 通知内容标题
         * @param msg_title          消息内容标题
         * @param msg_content        消息内容
         * @param extrasparam        扩展字段
         * @return 0推送失败，1推送成功
         */
        public static int sendToAll(String notification_title, String msg_title, String msg_content, String[] employeeIds, String extrasparam) {
            int result = 0;
            try {
                PushPayload pushPayload = JpushClientUtil.buildPushObject_android_and_ios(notification_title, msg_title, msg_content, employeeIds, extrasparam);
                System.out.println(pushPayload);
                PushResult pushResult = jPushClient.sendPush(pushPayload);
                System.out.println(pushResult);
                if (pushResult.getResponseCode() == 200) {
                    result = 1;
                }
            } catch (Exception e) {
    
                e.printStackTrace();
            }
    
            return result;
        }
    
        //all
        public static PushPayload buildPushObject_android_and_ios(String notification_title, String msg_title, String msg_content, String[] employeeIds, String extrasparam) {
            return PushPayload.newBuilder()
                    .setPlatform(Platform.android_ios())
                    .setAudience(Audience.alias(employeeIds))
                    .setNotification(Notification.newBuilder()
                            .setAlert("11111")
                            .addPlatformNotification(AndroidNotification.newBuilder()
                                    .setAlert(msg_content)
                                    .setTitle(msg_title)
                                    //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                    .addExtra("androidExt1", extrasparam)
                                    .build()
                            )
                            .addPlatformNotification(IosNotification.newBuilder()
                                    //传一个IosAlert对象，指定apns title、title、subtitle等
                                    .setAlert(IosAlert.newBuilder().setTitleAndBody(msg_title, null, msg_content).build())
                                    //直接传alert
                                    //此项是指定此推送的badge自动加1
                                    .incrBadge(1)
                                    //此字段的值default表示系统默认声音；传sound.caf表示此推送以项目里面打包的sound.caf声音来提醒，
                                    // 如果系统没有此音频则以系统默认声音提醒；此字段如果传空字符串，iOS9及以上的系统是无声音提醒，以下的系统是默认声音
                                    .setSound("sound.caf")
                                    //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                    .addExtra("iosExt1", extrasparam)
                                    //此项说明此推送是一个background推送，想了解background看：http://docs.jpush.io/client/ios_tutorials/#ios-7-background-remote-notification
                                    // .setContentAvailable(true)
                                    .build()
                            )
                            .build()
                    )
                    //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                    // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                    // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                    .setMessage(Message.newBuilder()
                            .setMsgContent(msg_content)
                            .setTitle(msg_title)
                            .addExtra("message extras key", extrasparam)
                            .build())
    
                    .setOptions(Options.newBuilder()
                            //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                            .setApnsProduction(false)
                            //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                            .setSendno(1)
                            //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
                            .setTimeToLive(86400)
                            .build()
                    )
                    .build();
        }
    
        private static PushPayload buildPushObject_all_registrationId_alertWithTitle(String registrationId, String notification_title, String msg_title, String msg_content, String extrasparam) {
    
            System.out.println("----------buildPushObject_all_all_alert");
            //创建一个IosAlert对象，可指定APNs的alert、title等字段
            //IosAlert iosAlert =  IosAlert.newBuilder().setTitleAndBody("title", "alert body").build();
    
            return PushPayload.newBuilder()
                    //指定要推送的平台，all代表当前应用配置了的所有平台，也可以传android等具体平台
                    .setPlatform(Platform.all())
                    //指定推送的接收对象，all代表所有人，也可以指定已经设置成功的tag或alias或该应应用客户端调用接口获取到的registration id
                    .setAudience(Audience.registrationId(registrationId))
                    //jpush的通知，android的由jpush直接下发，iOS的由apns服务器下发，Winphone的由mpns下发
                    .setNotification(Notification.newBuilder()
                            //指定当前推送的android通知
                            .addPlatformNotification(AndroidNotification.newBuilder()
    
                                    .setAlert(notification_title)
                                    .setTitle(notification_title)
                                    //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                    .addExtra("androidNotification extras key", extrasparam)
    
                                    .build())
                            //指定当前推送的iOS通知
                            .addPlatformNotification(IosNotification.newBuilder()
                                    //传一个IosAlert对象，指定apns title、title、subtitle等
                                    .setAlert(notification_title)
                                    //直接传alert
                                    //此项是指定此推送的badge自动加1
                                    .incrBadge(1)
                                    //此字段的值default表示系统默认声音；传sound.caf表示此推送以项目里面打包的sound.caf声音来提醒，
                                    // 如果系统没有此音频则以系统默认声音提醒；此字段如果传空字符串，iOS9及以上的系统是无声音提醒，以下的系统是默认声音
                                    .setSound("sound.caf")
                                    //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                    .addExtra("iosNotification extras key", extrasparam)
                                    //此项说明此推送是一个background推送，想了解background看：http://docs.jpush.io/client/ios_tutorials/#ios-7-background-remote-notification
                                    //取消此注释，消息推送时ios将无法在锁屏情况接收
                                    // .setContentAvailable(true)
    
                                    .build())


​    
                            .build())
                    //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                    // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                    // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                    .setMessage(Message.newBuilder()
    
                            .setMsgContent(msg_content)
    
                            .setTitle(msg_title)
    
                            .addExtra("message extras key", extrasparam)
    
                            .build())
    
                    .setOptions(Options.newBuilder()
                            //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                            .setApnsProduction(false)
                            //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                            .setSendno(1)
                            //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天；
                            .setTimeToLive(86400)
    
                            .build())
    
                    .build();
    
        }
    
        private static PushPayload buildPushObject_android_all_alertWithTitle(String notification_title, String msg_title, String msg_content, String extrasparam) {
            System.out.println("----------buildPushObject_android_registrationId_alertWithTitle");
            return PushPayload.newBuilder()
                    //指定要推送的平台，all代表当前应用配置了的所有平台，也可以传android等具体平台
                    .setPlatform(Platform.android())
                    //指定推送的接收对象，all代表所有人，也可以指定已经设置成功的tag或alias或该应应用客户端调用接口获取到的registration id
                    .setAudience(Audience.alias("17601018219"))//Audience.tag("tag1");Audience.all()
                    //jpush的通知，android的由jpush直接下发，iOS的由apns服务器下发，Winphone的由mpns下发
                    .setNotification(Notification.newBuilder()
                            //指定当前推送的android通知
                            .addPlatformNotification(AndroidNotification.newBuilder()
                                    .setAlert(notification_title)
                                    .setTitle(notification_title)
                                    //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                    .addExtra("androidNotification extras key", extrasparam)
                                    .build())
                            .build()
                    )
                    //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                    // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                    // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                    .setMessage(Message.newBuilder()
                            .setMsgContent(msg_content)
                            .setTitle(msg_title)
                            .addExtra("message extras key", extrasparam)
                            .build())
    
                    .setOptions(Options.newBuilder()
                            //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                            .setApnsProduction(false)
                            //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                            .setSendno(1)
                            //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
                            .setTimeToLive(86400)
                            .build())
                    .build();
        }
    
        private static PushPayload buildPushObject_ios_all_alertWithTitle(String notification_title, String msg_title, String msg_content, String extrasparam) {
            System.out.println("----------buildPushObject_ios_registrationId_alertWithTitle");
            return PushPayload.newBuilder()
                    //指定要推送的平台，all代表当前应用配置了的所有平台，也可以传android等具体平台
                    .setPlatform(Platform.ios())
                    //指定推送的接收对象，all代表所有人，也可以指定已经设置成功的tag或alias或该应应用客户端调用接口获取到的registration id
                    .setAudience(Audience.all())
                    //jpush的通知，android的由jpush直接下发，iOS的由apns服务器下发，Winphone的由mpns下发
                    .setNotification(Notification.newBuilder()
                            //指定当前推送的android通知
                            .addPlatformNotification(IosNotification.newBuilder()
                                    //传一个IosAlert对象，指定apns title、title、subtitle等
                                    .setAlert(notification_title)
                                    //直接传alert
                                    //此项是指定此推送的badge自动加1
                                    .incrBadge(1)
                                    //此字段的值default表示系统默认声音；传sound.caf表示此推送以项目里面打包的sound.caf声音来提醒，
                                    // 如果系统没有此音频则以系统默认声音提醒；此字段如果传空字符串，iOS9及以上的系统是无声音提醒，以下的系统是默认声音
                                    .setSound("sound.caf")
                                    //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                    .addExtra("iosNotification extras key", extrasparam)
                                    //此项说明此推送是一个background推送，想了解background看：http://docs.jpush.io/client/ios_tutorials/#ios-7-background-remote-notification
                                    // .setContentAvailable(true)
    
                                    .build())
                            .build()
                    )
                    //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                    // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                    // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                    .setMessage(Message.newBuilder()
                            .setMsgContent(msg_content)
                            .setTitle(msg_title)
                            .addExtra("message extras key", extrasparam)
                            .build())
    
                    .setOptions(Options.newBuilder()
                            //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                            .setApnsProduction(false)
                            //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                            .setSendno(1)
                            //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
                            .setTimeToLive(86400)
                            .build())
                    .build();
        }

实际调用方法：

    JpushClientUtil.sendToAllAndroid("testIos","testIos","this is a ios Dev test","")


​	
### 参考 ###

 1. [极光官方API](https://docs.jiguang.cn/jpush/guideline/intro/)

## 总结 ##
极光的API总体来说是用起来是比较简单的，之所以记下来是因为在网上看到了千篇一律的错误文章，不知道是因为极光版本升级了还是其他原因。
