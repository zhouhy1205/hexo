---
title: 远程调试(一)：在IDEA中远程调试Jar包
date: 2017-08-21 16:04:12
updated: 2017-08-21 16:04:12
tags: [Remote,Idea,Jar]
categories: Debug
description: "因为现在用Spring Boot 的工程多了起来，有时候用Spring Boot写的小脚本部署在服务器上出现独有的错误（开发环境正常）时，并不是那么好定位，所以用了一下远程调试Jar包，发现意外的好用，所以记录一下过程。"
---
## 前言 ##
***
因为现在用Spring Boot 的工程多了起来，有时候用Spring Boot写的小脚本部署在服务器上出现独有的错误（开发环境正常）时，并不是那么好定位，所以用了一下远程调试Jar包，发现意外的好用，所以记录一下过程。

## 正文 ##
### 配置IDEA ###
点击`+` —>选择`Remote`
![新建Remote配置](RemoteDebugging1JarPackageInIdea/new-remote.jpg)
配置远程服务器的ip与端口号，因为我的Jar是在本地启动的，所以是localhost
![配置](RemoteDebugging1JarPackageInIdea/config.jpg)

### 启动Jar ###

`address` 是远程服务的端口，也就是刚才配置的8081，`xxxxxx.jar` Jar包名字

     java -Xdebug -Xrunjdwp:transport=dt_socket,address=8081,server=y,suspend=y -jar xxxxxx.jar

启动后，会在以下界面卡住，此时正在监听8081端口，等待IDEA启动调试。

    E:\Run>java -Xdebug -Xrunjdwp:transport=dt_socket,address=8081,server=y,suspend=y -jar gateway.jar --spring.profiles.active=pro
    Listening for transport dt_socket at address: 8081

### 调试 ###
![启动调试](RemoteDebugging1JarPackageInIdea/start-debug.jpg)
启动调试后，服务器启动Jar界面会出现 ，项目启动日志，如果没有，说明没有监听到。

### 效果 ###
请求一下接口，发现打的断点生效了。
![效果](RemoteDebugging1JarPackageInIdea/res.jpg)