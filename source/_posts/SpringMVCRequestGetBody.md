---
title: SpringMvc：Request获取Body问题
date: 2017-10-22 13:42:38
updated: 2017-10-22 13:42:38
tags: [Request,HttpServletRequestWrapper]
categories: SpringMvc
description: "拦截器的使用很多时候是必不可少的，当有需要你对请求体（request.body）中的值进行校验，例如加密验签、防重复提交、内容校验等等。当你开开心心的在拦截器中通过request.getInputStream();获取到body中的信息后，你会发现controller再次获取会报错，这里的流是不能二次读取的，否则就会报Stream closed错误。"
---
## 问题 ##
现在开发的项目是基于`SpringBoot`的`maven`项目，拦截器的使用很多时候是必不可少的，当有需要你对请求体（request.body）中的值进行校验，例如加密验签、防重复提交、内容校验、token校验(如果你的token在body里面的话)等等。
当你开开心心的在拦截器中通过`request.getInputStream();`获取到body中的信息后，你会发现你在`controller`中使用了`@RequestBody`注解获取参数报如下错误:

    I/O error while reading input message; nested exception is java.io.IOException: Stream closed
    org.springframework.http.converter.HttpMessageNotReadableException: I/O error while reading input message; nested exception is java.io.IOException: Stream closed
    	at org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodArgumentResolver.readWithMessageConverters(AbstractMessageConverterMethodArgumentResolver.java:229)
    	at org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor.readWithMessageConverters(RequestResponseBodyMethodProcessor.java:150)
    	at org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor.resolveArgument(RequestResponseBodyMethodProcessor.java:128)
    	at org.springframework.web.method.support.HandlerMethodArgumentResolverComposite.resolveArgument(HandlerMethodArgumentResolverComposite.java:121)
    	at org.springframework.web.method.support.InvocableHandlerMethod.getMethodArgumentValues(InvocableHandlerMethod.java:158)
    	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:128)
    	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:97)
    	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:827)
    	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:738)
    	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:85)
    	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:967)
    	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:901)
    	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:970)
    	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:872)
    	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)
    	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:846)
    	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)
    	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)
    	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)
    	at org.springframework.boot.web.filter.ApplicationContextHeaderFilter.doFilterInternal(ApplicationContextHeaderFilter.java:55)
    	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)
    	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)
    	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)
    	at org.springframework.web.filter.CorsFilter.doFilterInternal(CorsFilter.java:96)
    	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)
    	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)
    	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)
    	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)
    	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)
    	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)
    	at org.springframework.boot.actuate.trace.WebRequestTraceFilter.doFilterInternal(WebRequestTraceFilter.java:110)
    	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)
    	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)
    	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)

## 原因 ##
我们希望从请求Request中获取输入流，解析里面的内容，但是`InputStream`只能被读取一次。为什么呢？看InputStream源码发现注释说的很清楚：

>public int read(byte[] b,int off, int len) 
　　　Reads up to len bytes of data into an array of bytes from this input stream. Ifpos equals count, then -1 is returned to 	 	 	
　　　indicate end of file. Otherwise, the number k of bytes read is equal to the smaller of len and count-pos.If k 
　　　is positive, then bytes buf[pos] through buf[pos+k-1] are copied into b[off] through b[off+k-1] in the manner performed by 
　　　System.arraycopy. The value k is added into pos and k is returned. 


>注释的大概意思是说：在InputStream读取的时候，会有一个pos 指针，它指示每次读取之后下一次要读取的起始位置。在每次读取后会更新pos的值，当你下次再来读取的时候是从pos的位置开始的，而不是从头开始，所以第二次获取String中的值的时候是不全的。如果第一次全部读取完了，pos就指向流的末尾，第二次就读取不到内容，API中提供了一个解决办法：reset()。`request、response中根本不起作用`。提示 mark/reset not supported 。意思是只有重写过markSupported()方法的IO流才可以用。所以一般我们使用inputStream，最好在一次内处理完所有逻辑。

## 方案 ##
IO流关闭只能读取一次，所以需要解决流只能读取一次的问题，让它可以被多次重复读取，这里只需要重写Request缓存一下流中的数据就好了。

### 引包 ###

    <dependency>
    	<groupId>org.apache.tomcat.embed</groupId>
    	<artifactId>tomcat-embed-core</artifactId>
    	<version>8.5.15</version>
    </dependency>

### 新建帮助类  ###
新建HttpUtil类 从流中读取body


    public class HttpUtil {
    	public static String getBodyString(HttpServletRequest request) throws IOException {
            StringBuilder sb = new StringBuilder();
            InputStream inputStream = null;
            BufferedReader reader = null;
            try {
                inputStream = request.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return sb.toString();
        }
    }

### 新建类Request包装类###
新建RequestReaderHttpServletRequestWrapper（防止流丢失）

	import com.zbsoft.common.util.HttpUtil;
	
	import java.io.BufferedReader;
	import java.io.ByteArrayInputStream;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.nio.charset.Charset;
	
	import javax.servlet.ReadListener;
	import javax.servlet.ServletInputStream;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletRequestWrapper;
	
	public class RequestReaderHttpServletRequestWrapper extends HttpServletRequestWrapper {
	
	private final byte[] body;
	
	public RequestReaderHttpServletRequestWrapper(HttpServletRequest request) throws IOException {
	    super(request);
	    body = HttpUtil.getBodyString(request).getBytes(Charset.forName("UTF-8"));
	}
	
	@Override
	public BufferedReader getReader() throws IOException {
	    return new BufferedReader(new InputStreamReader(getInputStream()));
	}
	
	@Override
	public ServletInputStream getInputStream() throws IOException {
	
	    final ByteArrayInputStream bais = new ByteArrayInputStream(body);
	
	    return new ServletInputStream() {
	
	        @Override
	        public int read() throws IOException {
	            return bais.read();
	        }
	
	        @Override
	        public boolean isFinished() {
	            return false;
	        }
	
	        @Override
	        public boolean isReady() {
	            return false;
	        }
	
	        @Override
	        public void setReadListener(ReadListener readListener) {
	
	        }
	    };
	}
}

### 新建过滤器 ###
新建HttpServletRequestReplacedFilter(过滤器)


    import javax.servlet.*;
    import javax.servlet.annotation.WebFilter;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import java.io.IOException;
    
    @WebFilter(urlPatterns = "/*", filterName = "channelFilter")
    public class HttpServletRequestReplacedFilter implements Filter {
        @Override
        public void destroy() {
        }
    
        @Override
        public void doFilter(ServletRequest request, ServletResponse response,
                             FilterChain chain) throws IOException, ServletException {
            ServletRequest requestWrapper = null;          
            if (request instanceof HttpServletRequest) {
                requestWrapper = new RequestReaderHttpServletRequestWrapper((HttpServletRequest) request);
            }
    
            //获取请求中的流如何，将取出来的字符串，再次转换成流，然后把它放入到新request对象中。
            // 在chain.doFiler方法中传递新的request对象
    
            if (requestWrapper != null && responseWrapper == null){
                chain.doFilter(requestWrapper, response);
            }
        }
    
        @Override
        public void init(FilterConfig arg0) throws ServletException {
    
        }
    }

## 结果 ##
如下代码即可在拦截其中获取body且保证了controller中依旧可以再次获取

     HttpUtil.getBodyString(request);
