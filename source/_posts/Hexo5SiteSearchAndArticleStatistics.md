---
title: Hexo系列（五）：站内搜索与文章统计
date: 2017-04-16 12:31:23
updated: 2017-04-16 12:31:23
tags: [Search]
categories: Hexo
description: "本来想用百度站内搜索，但是没成功，然后改用swiftype，但是swiftype需要注册账号并且只能用30天所以也放弃了，本文就简单的介绍一些yelee自带的站内搜索以及文章字数与阅读时长统计。"
---

## 前言 ##
>本来想用百度站内搜索，但是没成功，然后改用swiftype，但是swiftype需要注册账号并且只能用30天所以也放弃了，本文就简单的介绍一些yelee自带的站内搜索以及文章字数与阅读时长统计。

## 正文 ##
### 本地搜索 ###

使用搜索需先安装对应插件，用于生成索引数据
>插件主页: [hexo-generator-search](https://github.com/wzpan/hexo-generator-search)

`#on: true` 改为 `on: true`即为启用搜索

    search: 
      on: true
      onload: false

`onload: true` : 索引数据 `search.xml` 随页面一起加载 [效率优先]

`onload: false` : 当激活搜索框时再下载索引数据 [按需加载]
![本地搜索效果](Hexo5SiteSearchAndArticleStatistics/站内搜索.jpg)

### 文章统计 ###

`next`主题是已经集成这个功能的，但是`yelee`就需要我们自己配置了，可以看下官网对[hexo-wordcount](https://www.npmjs.com/package/hexo-wordcount)的介绍

#### 集成 ####
> 安装hexo-wordcount
> 

    npm i --save hexo-wordcount

#### 文件配置 ####
>在yelee/layout/_partial/post下创建word.ejs文件：

    <div style="margin-top:10px;">
        <span class="post-time">
          <span class="post-meta-item-icon">
            <i class="fa fa-keyboard-o"></i>
            <span class="post-meta-item-text">  字数统计: </span>
            <span class="post-count"><%= wordcount(post.content) %>字</span>
          </span>
        </span>
    
        <span class="post-time">
          &nbsp; | &nbsp;
          <span class="post-meta-item-icon">
            <i class="fa fa-hourglass-half"></i>
            <span class="post-meta-item-text">  阅读时长: </span>
            <span class="post-count"><%= min2read(post.content) %>分</span>
          </span>
        </span>
    </div>

>然后添加逻辑判断

打开 themes/yelee/layout/_partial/article.ejs

    <% if(theme.word_count && !post.no_word_count){ %>
        <%- partial('post/word') %>
    <% } %>

在下图位置添加
![插入位置](Hexo5SiteSearchAndArticleStatistics/插入位置.jpg)
`word_count` 是主题`_config.yml`中配置是否需要添加字数统计功能控制 flag，

`no_word_count`即配置文章是否需要显示字数统计的功能。

    # 是否开启字数统计
    #不需要使用，直接设置值为false，或注释掉
    word_count: true
    no_word_count: true

如果统计与标签重合或者位置不对，可以在`yelee/layout/_partial/post` 下面的的`word.ejs`与`tag.ejs` 设置`margin`属性
