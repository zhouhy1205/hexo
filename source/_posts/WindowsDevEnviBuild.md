---
title: Windows开发环境搭建
date: 2017-06-04 14:03:04
updated: 2006-06-14 14:03:04
tags: [Soft,Dev]
categories: Windows
description: 因为最近重装系统比较频繁，经常需要搭建开发环境，虽然搭建环境很简单但是比较繁琐，一些配置的字符串还需要google一下，太耽误时间，所以记录一下基本的开发环境搭建和常用软件。
---



## 前言

因为最近重装系统比较频繁，经常需要搭建开发环境，虽然搭建环境很简单但是比较繁琐，一些配置的字符串还需要google一下，太耽误时间，所以记录一下基本的开发环境搭建和常用软件。



## 正文

### JDK

#### 下载

在Oracle官网[下载JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

Oracle 账号

```
4670049@qq.com
1QAZ2wsx!
```

下载完成后安装一路Next

#### 配置环境变量

添加

```
 JAVA_HOME=E:\software\Java\jdk1.8.0_171
 CLASSPAT=.;%JAVA_HOME%\lib;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar
```

在path变量中添加

```
%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin
```

### Maven

#### 下载

[下载地址](https://maven.apache.org/download.cgi)

选择apache-maven-3.6.3-bin.zip下载

#### 解压

解压后的文件夹apache-maven-3.6.3移动到C:\Apps\

#### 配置环境变量

新建环境变量MAVEN_HOME，赋值C:\Apps\apache-maven-3.6.3

编辑环境变量Path，追加%MAVEN_HOME%\bin



指定C:\Apps\apache-maven-3.6.3\conf\settings中的仓库位置