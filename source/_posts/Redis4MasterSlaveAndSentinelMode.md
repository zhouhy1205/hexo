---
title: 关于Redis(四)：主从模式与哨兵模式
date: 2020-05-06 10:00:52
updated: 2020-05-06 10:00:52
tags: [Redis]
categories: NoSql
description: "前面几篇文章记录了单例Redis相关的一些应用，但是单例Redis存在不稳定性，读写能力也比较有限，当Redis服务宕机了，就没有可用的服务了。而Redis Cluster集群模式通常具有高可用、可扩展性、分布式、容错等特性。"
---

## 前言 ##

Redis后续拖了好久，其实笔记写了一年多，一直没有整理。

前面几篇文章记录了单例Redis相关的一些应用，但是单例Redis存在不稳定性，读写能力也比较有限，当Redis服务宕机了，就没有可用的服务了。而Redis Cluster集群模式通常具有高可用、可扩展性、分布式、容错等特性。

而Redis集群的高可靠性达到99.99%，也就是4个9。

> 在软件系统的高可靠性(也称为可用性，英文描述为HA，High Available)里有个衡量其可靠性的标准——X个9，这个X是代表数字3~5。X个9表示在软件系统1年时间的使用过程中，系统可以正常使用时间与总时间(1年)之比，我们通过下面的计算来感受下X个9在不同级别的可靠性差异。
>
> - 3个9：(1-99.9%)36524=8.76小时，表示该软件系统在连续运行1年时间里最多可能的业务中断时间是8.76小时。
> - 4个9：(1-99.99%)36524=0.876小时=52.6分钟，表示该软件系统在连续运行1年时间里最多可能的业务中断时间是52.6分钟。
> - 5个9：(1-99.999%)3652460=5.26分钟，表示该软件系统在连续运行1年时间里最多可能的业务中断时间是5.26分钟。
> - 1个9：(1-90%)365=36.5天
> - 2个9：(1-99%)365=3.65天
> - 6个9：(1-99.9999%)3652460*60=31秒

## 正文 ##

### 集群的三种模式 ###

1. 主从模式 (主从复制)
2. Sentinel模式(哨兵模式)
3. Cluster模式



### 主从模式(主从复制)

#### **模式介绍**

主从复制模型中，有多个redis节点。

其中，有且仅有一个为主节点Master。从节点Slave可以有多个。

只要网络连接正常，Master会一直将自己的数据更新同步给Slaves，保持主从同步。



#### **特点**

```shell
#主数据库可以进行读写操作，当读写操作导致数据变化时会自动将数据同步给从数据库。

#从数据库一般都是只读的，并且接收主数据库同步过来的数据。

#一个master可以拥有多个slave，但是一个slave只能对应一个master。

#slave挂了不影响其他slave的读和master的读和写，重新启动后会将数据从master同步过来。

#master挂了以后，不影响slave的读，但redis不再提供写服务，master重启后redis将重新对外提供写服务。
```

因此，主从模型可以提高读的能力，在一定程度上缓解了写的能力。因为能写仍然只有Master节点一个，可以将读的操作全部移交到从节点上，间接的提高了写能力。

![主从模式](Redis4MasterSlaveAndSentinelMode/ms.png)

#### **工作机制**

当slave启动后，主动向master发送SYNC命令。master接收到SYNC命令后在后台保存快照（RDB持久化）和缓存保存快照这段时间的命令，然后将保存的快照文件和缓存的命令发送给slave。slave接收到快照文件和命令后加载快照文件和缓存的执行命令。

复制初始化后，master每次接收到的写命令都会同步发送给slave，保证主从数据一致性。



#### **部署**

**主节点直接使用默认配置启动**。

**从节点修改完一下配置直接启动。**

**从节点配置文件**

```shell
#cp /etc/redis.conf /etc/redis2.conf

#不能与主冲突
port 6380

#pid文件不能跟Master节点相同
pidfile /var/run/redis_6380.pid

#日志文件不能跟Master节点相同
logfile "/var/log/redis2.log"

#dir不能跟Master节点相同
dir /data/redis2

#说明它是哪个的从，salveof 主IP 主服务端口
slaveof 127.0.0.1 6379

#设置从的密码，假设主的密码为123123
masterauth 123123
```

Slave节点不需要手动去同步数据，它会自动同步主上面的数据。



#### **缺陷**

Master节点在主从模式中唯一，若master挂掉，则redis无法对外提供写服务。

由于Slave节点上备份了Master节点的所有数据，如果Master节点宕机的情况下Slave节点能够变成Master节点，那么这个问题就能很好的解决了，Sentinel哨兵模式就是实现这种方式的集群。



### Sentinel模式(哨兵模式)

#### **模式介绍**

Sentinel（哨兵）是Redis的高可用性解决方案：由一个或多个Sentinel实例组成的Sentinel系统可以监视任意多个主服务器，以及这些主服务器属下的所有从服务器，并在被监视的主服务器进入下线状态时，自动将下线主服务器属下的某个从服务器升级为新的主服务器。

#### **特点**

```shell
#sentinel模式是建立在主从模式的基础上，如果只有一个Redis节点，sentinel就没有任何意义。

#当master挂了以后，sentinel会在slave中选择一个做为master，并修改它们的配置文件，其他slave的配置文件也会被修改，比如slaveof属性会指向新的master

#当master重新启动后，它将不再是master而是做为slave接收新的master的同步数据

#sentinel因为也是一个进程有挂掉的可能，所以sentinel也会启动多个形成一个sentinel集群 * 多sentinel配置的时候，sentinel之间也会自动监控

#当主从模式配置密码时，sentinel也会同步将配置信息修改到配置文件中，不需要担心

#一个sentinel或sentinel集群可以管理多个主从Redis，多个sentinel也可以监控同一个redis

#sentinel最好不要和Redis部署在同一台机器，不然Redis的服务器挂了以后，sentinel也挂了
```

#### **工作机制**

```shell
#每个sentinel以每秒钟一次的频率向它所知的master，slave以及其他sentinel实例发送一个 PING 命令 

#如果一个实例距离最后一次有效回复 PING 命令的时间超过 down-after-milliseconds 选项所指定的值， 则这个实例会被sentinel标记为主观下线。

#如果一个master被标记为主观下线，则正在监视这个master的所有sentinel要以每秒一次的频率确认master的确进入了主观下线状态

#当有足够数量的sentinel（大于等于配置文件指定的值）在指定的时间范围内确认master的确进入了主观下线状态， 则master会被标记为客观下线

#在一般情况下， 每个sentinel会以每 10 秒一次的频率向它已知的所有master，slave发送 INFO 命令

#当master被sentinel标记为客观下线时，sentinel向下线的master的所有slave发送 INFO 命令的频率会从 10 秒一次改为 1 秒一次 

#若没有足够数量的sentinel同意master已经下线，master的客观下线状态就会被移除；  若master重新向sentinel的 PING 命令返回有效回复，master的主观下线状态就会被移除
```

当使用sentinel模式的时候，客户端就不要直接连接Redis，而是连接sentinel的ip和port，由sentinel来提供具体的可提供服务的Redis实现，这样当master节点挂掉以后，sentinel就会感知并将新的master节点提供给使用者。



#### **哨兵的工作**

Redis 的 Sentinel 系统用于管理多个 Redis 服务器， 该系统执行以下三个任务：

- 监控(Monitoring)：Sentinel会不断地检查你的主服务器和从服务器是否运作正常。
- 提醒(Notification)：当被监控的某个Redis服务器出现问题时，Sentinel可以通过API向管理员或者其他应用程序发送通知。
- 自动故障迁移(Automatic failover)：当一个主服务器不能正常工作时，Sentinel会开始一次自动故障迁移操作，它会进行选举，将其中一个从服务器升级为新的主服务器，并让失效主服务器的其他从服务器改为复制新的主服务器；当客户端试图连接失效的主服务器时，集群也会向客户端返回新主服务器的地址，使得集群可以使用新主服务器代替失效服务器。



**监控(Monitoring)**

- Sentinel可以监控任意多个Master和该Master下的Slaves。(即多个主从模式)
- 同一个哨兵下的、不同主从模型，彼此之间相互独立。
- Sentinel会不断检查Master和Slaves是否正常。




**自动故障切换(Automatic failover)**

**Sentinel网络**

监控同一个Master的Sentinel会自动连接，组成一个**分布式**的Sentinel网络，互相通信并交换彼此关于被监视服务器的信息。下图中，三个监控s1的Sentinel，自动组成Sentinel网络结构。



当只有一个sentinel的时候，如果这个sentinel挂掉了，那么就无法实现自动故障切换了,所以这里用了Sentunel网络。在sentinel网络中，只要还有一个sentinel活着，就可以实现故障切换。



**故障切换的过程**

(1)投票(半数原则)

当任何一个Sentinel发现被监控的Master下线时，会通知其它的Sentinel开会，投票确定该Master是否下线(半数以上，所以sentinel通常配奇数个)。

(2)选举

当Sentinel确定Master下线后，会在所有的Slaves中，选举一个新的节点，升级成Master节点。

其它Slaves节点，转为该节点的从节点。

(3)原Master重新上线

当原Master节点重新上线后，自动转为当前Master节点的从节点。



#### **部署**

仅提供关键配置，具体步骤略。

```shell
# vim /usr/local/redis/sentinel.conf
daemonize yes 
logfile "/usr/local/redis/sentinel.log" 
#sentinel工作目录
dir "/usr/local/redis/sentinel"
#判断master失效至少需要2个sentinel同意，建议设置为n/2+1，n为sentinel个数
sentinel monitor mymaster 192.168.30.128 6379 2
sentinel auth-pass mymaster 123456
#判断master主观下线时间，默认30s
sentinel down-after-milliseconds mymaster 30000
```



#### **缺陷**

哨兵模式不能动态扩充，所以Redis在3.x以后提出cluster集群模式。





## **总结**

因为主从模式对Mstaer节点过于依赖，所以有了哨兵模式。

但是哨兵模式不能狗动态扩充，所以有了Redis Cluster模式。

Redis Cluster模式可以看作主从和哨兵的结合，下一篇将Redis Cluster模式。