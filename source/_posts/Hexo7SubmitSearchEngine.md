---
title: Hexo系列（七）：提交搜索引擎
date: 2017-05-28 15:27:46
updated: 2017-05-28 15:27:46
tags: [Baidu,Google,Seo]
categories: Hexo
description: "到了这一步，就博客的搭建基本已经完成了，细心的人就会发现一个问题，搭建的博客在baidu和google上搜索不到，这个问题真的是忍不了啊，完全不能愉快的装逼了，本文教大家做一下简单的SEO。"
---
## 前言 ## 
***
到了这一步，就博客的搭建基本已经完成了，细心的人就会发现一个问题，搭建的博客在`baidu`和`google`上搜索不到，这个问题真的是忍不了啊，完全不能愉快的装逼了，本文教大家做一下简单的SEO。

## 正文

### 查看收录情况 ###
***
在站点前面输入`site:`可以查看搜索引擎对该站点的收录情况。
![百度收录情况](Hexo7SubmitSearchEngine/baidu_site.png)
![谷歌收录情况](Hexo7SubmitSearchEngine/google_site.jpg)
从图中可以看出`baidu`和`google`对我的网站没有收录任何一个路径。

### 验证网站 ###
首先要确认网站是你本人所有，可以通过一下方式验证
>[百度搜索引擎入口](https://ziyuan.baidu.com/linksubmit/url)
>[为什么要验证网站](https://ziyuan.baidu.com/college/courseinfo?id=267&page=1#h2_article_title3)　　
>站长平台推荐站长添加主站（您网站的链接也许会使用www 和非 www 两种网址，建议添加用户能够真实访问到的网址），添加并验证后，可证明您是该域名的拥有者，可以快捷批量添加子站点，查看所有子站数据，无需再一一验证您的子站点。
>[如何验证网站](https://ziyuan.baidu.com/college/courseinfo?id=267&page=1#h2_article_title13)
>首先如果您的网站已使用了百度统计，您可以使用统计账号登录平台，或者绑定站长平台与百度统计账号，站长平台支持您批量导入百度统计中的站点，您不需要再对网站进行验证。
>百度站长平台为未使用百度统计的站点提供三种验证方式：文件验证、html标签验证、CNAME验证。
>　　1.文件验证：您需要下载验证文件，将文件上传至您的服务器，放置于域名根目录下。
>　　2.html标签验证：将html标签添加至网站首页html代码的标签与标签之间。
>　　3.CNAME验证：您需要登录域名提供商或托管服务提供商的网站，添加新的DNS记录。
>验证完成后，我们将会认为您是网站的拥有者。为使您的网站一直保持验证通过的状态，请保留验证的文件、html标签或CNAME记录，我们会去定期检查验证记录。



![google验证](Hexo7SubmitSearchEngine/google验证.jpg)

不管谷歌还是百度都要先添加域名，然后验证网站，这里统一都使用文件验证，就是下载对应的html文件，放到域名根目录下，也就收博客根目录下的source下面，当执行`hexo g`后会自动打包到`public`下面
![验证文件位置](Hexo7SubmitSearchEngine/check_file_address.jpg)

然后部署到服务器,输入地址：https://zhouhy.top/baidu_verify_zn1b0ZTy80.html 能访问到就可以点验证按钮。
![验证页面](Hexo7SubmitSearchEngine/check_page.jpg)

### 站点地图 ###
>站点地图是一种文件，您可以通过该文件列出您网站上的网页，从而将您网站内容的组织架构告知Google和其他搜索引擎。Googlebot等搜索引擎网页抓取工具会读取此文件，以便更加智能地抓取您的网站。

我们要先安装一下，打开hexo博客根目录，分别用下面两个命令来安装针对谷歌和百度的插件

    npm install hexo-generator-sitemap --save
    npm install hexo-generator-baidu-sitemap --save

在博客目录的_config.yml中添加如下代码

    # 自动生成sitemap
    sitemap:
    path: sitemap.xml

编译你的博客

    hexo g

如果你在你的博客根目录的public下面发现生成了`sitemap.xml`就表示成功了。
**注意：**github禁止了百度爬虫，提交了百度也是不会访问的。

部署后访问
http://zhouhy.top/sitemap.xml
效果如下图：
![sitemap.xml](Hexo7SubmitSearchEngine/site_page.jpg)
为了本地md文件容易阅读，我的md文件名字为中文，所以我的路径中也出现了中文（路径最好不要出现中文），类似：`http://yibierusi.github.io/2017/02/28/Hexo%E7%B3%BB%E5%88%97%EF%BC%88%E4%B8%83%EF%BC%89%EF%BC%9A%E6%8F%90%E4%BA%A4%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E/`

### 网站提交方式 ###
>如何选择链接提交方式
1、主动推送：最为快速的提交方式，推荐您将站点当天新产出链接立即通过此方式推送给百度，以保证新链接可以及时被百度收录。
2、自动推送：最为便捷的提交方式，请将自动推送的JS代码部署在站点的每一个页面源代码中，部署代码的页面在每次被浏览时，链接会被自动推送给百度。可以与主动推送配合使用。
3、sitemap：您可以定期将网站链接放到sitemap中，然后将sitemap提交给百度。百度会周期性的抓取检查您提交的sitemap，对其中的链接进行处理，但收录速度慢于主动推送。
4、手动提交：一次性提交链接给百度，可以使用此种方式。

一般主动提交比手动提交效果好。

从效率上来说：

>主动推送 > 自动推送 > sitemap


>为什么自动推送可以更快的将页面推送给百度搜索？基于自动推送的实现原理问题，当新页面每次被浏览时，页面URL会自动推送给百度，无需站长汇总URL再进行主动推送操作。
借助用户的浏览行为来触发推送动作，省去了站长人工操作的时间。
自动推送和链接提交有什么区别？已经在使用链接提交的网站还需要再部署自动推送代码吗？
二者之间互不冲突，互为补充。已经使用主动推送的站点，依然可以部署自动推送的JS代码，二者一起使用。
什么样的网站更适合使用自动推送？自动推送由于实现便捷和后续维护成本低的特点，适合技术能力相对薄弱，无能力支持全天候实时主动推送程序的站长。
站长仅需一次部署自动推送JS代码的操作，就可以实现新页面被浏览即推送的效果，低成本实现链接自动提交。
同时，我们也支持主动推送和自动推送代码配合使用，二者互不影响。

### 谷歌收录 ###
#### sitemap ####
谷歌操作比较简单，就是向[Google站长工具](https://www.google.com/webmasters/tools)提交`sitemap`

登录Google账号，添加了站点验证通过后，选择站点，`抓取`—>`站点地图`—>`添加/测试站点地图`，提交自己的`sitemap`就可以了
谷歌收录网站的效率非常高，提交上去一天左右就能搜索到了。

### 百度收录 ###
谷歌很好搞定，百度就很麻烦了，流程复杂，耗时长，收录效果差。
正常情况，是要等百度爬虫来爬到网站，才会被收录。
但是github屏蔽了百度爬虫目前，所以不能被动的等着百度来爬取，我们自己把网站提交给百度。
这就要使用到[百度站长平台](https://ziyuan.baidu.com/)

#### 自动推送 ####
自动推送很简单，就是在你代码里面嵌入自动推送JS代码，在页面被访问时，页面URL将立即被推送给百度
代码如下：

    <script>
        (function(){
            var bp = document.createElement('script');
            bp.src = '//push.zhanzhang.baidu.com/push.js';
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(bp, s);
        })();
    </script>

我是放在\themes\yelee\layout\_partial\after_footer.ejs中，添加到下面就行。

#### sitemap ####
`sitemap`的提交流程跟上面`google`一样，直接提交http://zhouhy.top/sitemap.xml 就行，注意sitemap中的路径，如果是github的路径百度是无法爬取的，需要改为zhouhy.top,这个就会自动跳转到coding的路径。
