---
title:  DeepinLinux自带VPN配置(L2TP/IpSec) 
date: 2019-11-22 16:38:18
updated: 2019-11-22 16:38:18
tags: [Vpn,L2tp]
categories: Deepin
description: "最近开始使用Linux-Deepin系统了，所以WIN10上的VPN也需要移到LINUX下面，然后搜索了一下，看网上都是让安装各种东西。所以自己试下，发现可以不用安装任何东西的配置方法。"
---
## 前言 ##
[WIN10 篇跳转链接](http://zhouhy.top/2018/02/18/win10-use-ca-L2TP-OR-IPSEC-vpn-config/)

最近开始使用Linux-Deepin系统了，所以WIN10上的VPN也需要移到LINUX下面，然后搜索了一下，看网上都是让安装各种东西。所以自己试下，发现可以不用安装任何东西的配置方法。

例如网上教程：
![](DeepinLinuxL2tpConfig/1.png)


## 第一步（最后一步） ##
Deepin中提供了多种VPN的配置方法，这里就只举例说一种。
![](DeepinLinuxL2tpConfig/0.png)

控制中心 `->` 网络`->`  VPN `->` 创建`->`  L2TP

![](DeepinLinuxL2tpConfig/2.png)


## 导出 ##
Deepin中VPN的配置还可以导出，方便二次配置
![](DeepinLinuxL2tpConfig/3.png)

