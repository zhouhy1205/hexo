---
title: Docker的介绍以及安装
date: 2020-04-09 16:27:34
updated: 2020-04-09 16:27:34
tags: [Docker]
categories: Linux
description: "Docker自2013年以来非常火热，无论是从 github 上的代码活跃度，还是Redhat在RHEL6.5中集成对Docker的支持, 就连 Google 的 Compute Engine 也支持 docker 在其之上运行。所以运维及后端开发还是非常有必要来了解一下的。"
---



## 前言

​	Docker 是 [PaaS](https://baike.baidu.com/item/PaaS) 提供商 dotCloud 开源的一个基于 [LXC](https://baike.baidu.com/item/LXC) 的高级容器引擎，源代码托管在 [Github](https://baike.baidu.com/item/Github) 上, 基于[go语言](https://baike.baidu.com/item/go%E8%AF%AD%E8%A8%80)并遵从Apache2.0协议开源。

​	Docker自2013年以来非常火热，无论是从 github 上的代码活跃度，还是Redhat在RHEL6.5中集成对Docker的支持, 就连 Google 的 Compute Engine 也支持 docker 在其之上运行。所以运维及后端开发还是非常有必要来了解一下的。

## 正文

### 介绍	

Docker 是一个开源的应用容器引擎，基于 [Go 语言](https://www.runoob.com/go/go-tutorial.html) 并遵从 Apache2.0 协议开源。 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

### Docker的应用场景

- Web 应用的自动化打包和发布。
- 自动化测试和持续集成、发布。
- 在服务型环境中部署和调整数据库或其他的后台应用。
- 从头编译或者扩展现有的 OpenShift 或 Cloud Foundry 平台来搭建自己的 PaaS 环境。



### Docker 的优点

Docker 是一个用于开发，交付和运行应用程序的开放平台。Docker 使您能够将应用程序与基础架构分开，从而可以快速交付软件。借助 Docker，您可以与管理应用程序相同的方式来管理基础架构。通过利用 Docker 的方法来快速交付，测试和部署代码，您可以大大减少编写代码和在生产环境中运行代码之间的延迟。

#### 快速、一致地交付应用程序

Docker 允许开发人员使用你提供的应用程序或服务的本地容器在标准化环境中工作，从而简化了开发的生命周期。

容器非常适合持续集成和持续交付（CI / CD）工作流程，请考虑一下示例方案：

- 开发人员在本地编写代码，并使用 Docker 容器与同事共享他们的工作。
- 他们使用 Docker 将其应用程序推送到测试环境中，并执行自动或手动测试。
- 当开发人员发现错误时，他们可以在开发环境中对其进行修复，然后将其重新部署到测试环境中，以进行测试和验证。
- 测试完成后，将修补程序推送给生产环境，就像将更新的镜像推送到生产环境一样简单。

#### 响应式部署和扩展

Docker 是基于容器的平台，允许高度可移植的工作负载。Docker 容器可以在开发人员的本机上，数据中心的物理或虚拟机上，云服务上或混合环境中运行。

Docker 的可移植性和轻量级的特性，还可以使您轻松地完成动态管理的工作负担，并根据业务需求指示，实时扩展或拆除应用程序和服务。

#### 在同一硬件上运行更多工作负载

Docker 轻巧快速。它为基于虚拟机管理程序的虚拟机提供了可行、经济、高效的替代方案，因此您可以利用更多的计算能力来实现业务目标。Docker 非常适合于高密度环境以及中小型部署，而您可以用更少的资源做更多的事情。



### Docker的安装

#### 安装环境

操作系统：Deepin 15.11

软件源：阿里云镜像（在阿里云镜像站上面可以找到docker-ce的软件源，使用国内的源速度比较快）



#### 安装方式

Docker的安装有三种安装方式分别是：

1. 使用存储库安装
2. 使用安装包安装
3. 使用便捷脚本安装

 

**此处用的是第一种安装方式。**

#### 设置Docker存储库

使用以下命令更新apt包索引

```shell
sudo apt-get update
```

安装以下包以允许apt通过HTTPS使用存储库

```shell
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

安装完成后，在输入一遍上面的命令，就出现都已经是最新版。

 

添加Docker的官方GPG密钥并载入密钥

```shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

 

设置稳定的存储库（添加软件源）

在`/etc/apt/sources.list`文件中的文末添加官方源()

```shell
#deepin系统
deb [arch=amd64] https://download.docker.com/linux/debian stretch stable
```

#### 安装Docker

更新apt包索引

```shell
sudo apt-get update
```

安装最新版本的Docker CE和containerd

```shell
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

现在我们来通过运行hello-world映像验证是否正确安装Docker

```shell
sudo docker run hello-world
```

运行时本地没有hello-world镜像时会自动下载，镜像名后可以追加版本号，用冒号分割，如果不追加，默认下载最新版本。



出现下图所示提示，则安装成功。

![Docker安装成功图](DockerIntroduceAndInstall/Docker安装成功图.png)



### Docker常用命令

```shell
#启动/停止/重启 
systemctl start/stop/restart docker
service docker restart

#非Root用户使用docker命令 zhou 为用户名
sudo usermod -aG docker zhou

#版本
docker -v

#信息
docker info
docker inspect <容器ID>

#查看所有的容器
docker ps -a

#启动一个已停止的容器
docker start <容器 ID/别名> 

#停止/重启一个容器
docker stop/restart <容器 ID/别名>

#删除容器 -f 运行时强制删除
docker rm -f <容器 ID>

#查看镜像
docker images

#删除镜像
docker rmi <镜像ID/名字>

#后台运行容器 -d
docker run -d --name <容器别名> <镜像名字>:<版本号>

#host网络启动--net host 不需要映射端口，容器端口就是宿主机端口
#--restart always 总是启动
docker run --name jenkins -d --net host --restart always jenkins

#在使用 -d 参数时，容器启动后会进入后台。此时想要进入容器，可以通过以下指令进入。
#推荐使用 docker exec 命令，因为此退出容器终端，不会导致容器的停止。
#docker attach
docker attach <容器ID/别名> 
#docker exec
docker exec -it <容器 ID/别名> /bin/bash


#导入导出容器
docker export <容器 ID/别名> > <导出的容器名字 例如 ubuntu.tar>
cat docker/ubuntu.tar | docker import - test/ubuntu:v1

#手动编译镜像 （注意最后面有个"."点）
#在服务器新建一个docker文件夹，将maven打包好的jar包和Dockerfile文件复制到服务器的docker文件夹下
docker build -t 镜像名字 .

#启动
docker run -d -p 8080:8085 springbootdemo4docker
#-d参数是让容器后台运行 
#-p 是做端口映射，此时将服务器中的8080端口映射到容器中的8085(项目中端口配置的是8085)端口

#宿主机挂载到容器。
docker run -it -v <宿主主机目录>:<容器对应目录> <镜像名字> /bin/bash

#将宿主主机目录和容器目录建立映射关系 <有问题>
#docker run --name <镜像别名> -v <宿主主机目录>:<容器对应目录> <镜像名字>:<版本号>

#查看启动日志 -f 实时查看
docker logs -f <容器ID/容器名字/容器别名>

#容器（必须绝对路径）->宿主机
docker cp <容器ID/名字/别名>:<容器主机文件或文件夹（绝对路径)> <宿主机路径>

#宿主机->容器（必须绝对路径）拷贝  
docker cp <宿主机文件或文件夹> <容器ID/名字/别名>:<容器主机文件夹>
```

