---
title: Win10自带VPN配置(L2TP/IpSec)
date: 2018-02-18 13:42:38
updated: 2018-02-18 13:42:38
tags: [L2tp/IpSec]
categories: VPN
description: "VPN(虚拟专用网)发展至今已经不在是一个单纯的经过加密的访问隧道了，它已经融合了访问控制、传输管理、加密、路由选择、可用性管理等多种功能，并在全球的信息安全体系中发挥着重要的作用。"
---

## 前言 ##

> VPN(虚拟专用网)发展至今已经不在是一个单纯的经过加密的访问隧道了，它已经融合了访问控制、传输管理、加密、路由选择、可用性管理等多种功能，并在全球的信息安全体系中发挥着重要的作用。
> 

以前连接内网VPN时需要使用VPN厂家提供的软件，一般这种VPN软件极其古老，兼容性差，易掉线等各种问题，我以前用Win10系统时会在VM中装一个Win XP系统来配合使用，非常繁琐。本文介绍Win 10自带的VPN的配置。

Win10 自带的VPN 隧道协议：PPTP（点对点隧道协议）、L2TP（第2层隧道协议）、IPSec、SSLVPN。


[Linux-Deepin篇跳转链接](http://zhouhy.top/2019/11/22/linux-deepin-l2tp-config/)


## L2TP/IpSec 配置
1.设置 `->` 网络和Internet `->` VPN `->` 添加VPN连接
![](Win10ConfigL2tpORIpecVpn/1.png)
2.1.设置 `->` 网络和Internet `->` VPN `->` 更改设配器选项
![](Win10ConfigL2tpORIpecVpn/2.png)
3.选择刚才新建的VPN右键 `->` 属性
![](Win10ConfigL2tpORIpecVpn/3.png)
![](Win10ConfigL2tpORIpecVpn/4.png)
![](Win10ConfigL2tpORIpecVpn/5.png)
![](Win10ConfigL2tpORIpecVpn/6.png)
![](Win10ConfigL2tpORIpecVpn/7.png)
![](Win10ConfigL2tpORIpecVpn/8.png)


###连接失败解决方法 ### 

错误描述：“L2TP连接尝试失败，因为安全层在初始化与远程计算机的协商时遇到了一个处理错误”

解决办法：
	1、Windows + r（ 即开始运行） 在框中输入 regedit ， 打开注册表

	2、找到\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\RasMan\Parameters位置
	
	3、新建  DWORD ，在键的位置输入ProhibitIpSec ，并修改其数值数据为1
	
	4、修改 键 AllowL2TPWeakCrypto 的 数值数据 为 1 
	
	5、重启电脑
