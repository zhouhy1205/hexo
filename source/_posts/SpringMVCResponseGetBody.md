---
title: SpringMvc：Response获取Body问题
date: 2017-11-03 13:42:38
updated: 2017-11-03 13:42:38
tags: [Response,HttpServletResponseWrapper]
categories: SpringMvc
description: "拦截器的使用很多时候是必不可少的，当你需要对请求的body与返回的body进行记录的时候，在`afterCompletion`中从response中获取流，读取body入库后，你会发现前端获取不到后端传过去的body。"
---
## 问题 ##
***
现在开发的项目是基于SpringBoot的maven项目，拦截器的使用很多时候是必不可少的，当你需要对请求的body与返回的body进行记录的时候，在`afterCompletion`中从response中获取流，读取body入库后，你会发现前端获取不到后端传过去的body。


## 原因 ##
***
这个问题跟requst 一模一样，情况在[上篇博客](http://zhouhy.top/2018/03/02/Request%E8%8E%B7%E5%8F%96Body%E9%97%AE%E9%A2%98/)中有详细的说明

## 方案 ##
***
IO流关闭只能读取一次，所以需要解决流只能读取一次的问题，让它可以被多次重复读取，这里只需要重写Response缓存一下流中的数据就好了。

### 引包 ###

    <dependency>
    	<groupId>org.apache.tomcat.embed</groupId>
    	<artifactId>tomcat-embed-core</artifactId>
    	<version>8.5.15</version>
    </dependency>

### 新建类Response包装类###
新建BodyCachingHttpServletResponseWrapper（防止流丢失）

    import lombok.AllArgsConstructor;
    import lombok.Data;
    
    import javax.servlet.ServletOutputStream;
    import javax.servlet.WriteListener;
    import javax.servlet.http.HttpServletResponse;
    import javax.servlet.http.HttpServletResponseWrapper;
    import java.io.ByteArrayOutputStream;
    import java.io.IOException;
    import java.io.OutputStreamWriter;
    import java.io.PrintWriter;
    
    /**
     * @author
     * @date 2018/10/1
     */
    public class BodyCachingHttpServletResponseWrapper extends HttpServletResponseWrapper {
    
        private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        private HttpServletResponse response;
    
        public BodyCachingHttpServletResponseWrapper(HttpServletResponse response) {
            super(response);
            this.response = response;
        }
        //获取包装前的response
        public HttpServletResponse getNativeResponse(){
            return this.response;
        }
        public byte[] getBody() {
            return byteArrayOutputStream.toByteArray();
        }
    
        @Override
        public ServletOutputStream getOutputStream() {
            return new ServletOutputStreamWrapper(this.byteArrayOutputStream , this.response);
        }
    
        @Override
        public PrintWriter getWriter() throws IOException {
            return new PrintWriter(new OutputStreamWriter(this.byteArrayOutputStream , this.response.getCharacterEncoding()));
        }


​    
        @Data
        @AllArgsConstructor
        private static class ServletOutputStreamWrapper extends ServletOutputStream {
    
            private ByteArrayOutputStream outputStream;
            private HttpServletResponse response;
    
            @Override
            public boolean isReady() {
                return true;
            }
    
            @Override
            public void setWriteListener(WriteListener listener) {
    
            }
    
            @Override
            public void write(int b) throws IOException {
                this.outputStream.write(b);
            }
    
            @Override
            public void flush() throws IOException {
                if (! this.response.isCommitted()) {
                    byte[] body = this.outputStream.toByteArray();
                    ServletOutputStream outputStream = this.response.getOutputStream();
                    outputStream.write(body);
                    outputStream.flush();
                }
            }
        }
    }

### 新建过滤器 ###
新建HttpServletResponseReplacedFilter(过滤器)


    import javax.servlet.*;
    import javax.servlet.annotation.WebFilter;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import java.io.IOException;
    
    @WebFilter(urlPatterns = "/*", filterName = "channelFilter")
    public class HttpServletRequestReplacedFilter implements Filter {
        @Override
        public void destroy() {
        }
    
        @Override
        public void doFilter(ServletRequest request, ServletResponse response,
                             FilterChain chain) throws IOException, ServletException {
            BodyCachingHttpServletResponseWrapper responseWrapper = null;
            if (response instanceof HttpServletResponse){
                responseWrapper  = new BodyCachingHttpServletResponseWrapper((HttpServletResponse) response);
            }
    
            //获取请求中的流如何，将取出来的字符串，再次转换成流，然后把它放入到新request对象中。
            // 在chain.doFiler方法中传递新的request对象
            //此处response包装为responseWrapper 后 通过response.getWriter().print(str);传值是 前台无法接收
            if (requestWrapper == null && responseWrapper != null){
                chain.doFilter(request, responseWrapper);
            }
        }
    
        @Override
        public void init(FilterConfig arg0) throws ServletException {
    
        }
    }

## 结果 ##
***
如下代码即可在拦截其中获取body且保证了controller中依旧可以再次获取

    byte[] body = ((BodyCachingHttpServletResponseWrapper) res).getBody();
    java.lang.String string = new java.lang.String(body);


## 注意 ##
***
在拦截器的`preHandle`方法中返回true的请求，会进入到对应的Controller，在Controller中返回的对象有Spring Boot框架自动转换为JSON，放入response的body中，这个步骤是没有问题的。

但是当在拦截器中`preHandle`方法中返回false的请求,不会经由Controller赋予返回值，应该由程序员手动赋值。
常用的方法如下：

    PrintWriter writer = response.getWriter();
    writer.print(JSON.toJSONString(Result.create(MsgConstant.NOT_LOGIN)));
    writer.flush();
    writer.close();
实际上，此时的response是经过包装后的，它的实际类型还是`BodyCachingHttpServletResponseWrapper`，这个方式前端是接收不到值的。需要把BodyCachingHttpServletResponseWrapper 转化为HttpServletResponse，**而不是父类引用指向子类对象**。

我在`BodyCachingHttpServletResponseWrapper`中写了如下方法，目的就是获取HttpServletResponse类型的response

    //获取包装前的response
    public HttpServletResponse getNativeResponse(){
        return this.response;
    }

此处返回值的代码如下：

        response = ((BodyCachingHttpServletResponseWrapper)response).getNativeResponse();
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(Result.create(MsgConstant.JSON_INVALID_FORMAT)));
        writer.flush();
        writer.close();
