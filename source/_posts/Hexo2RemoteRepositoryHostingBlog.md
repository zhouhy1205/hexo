---
title: Hexo系列（二）：远程仓库托管博客
date: 2017-03-12 15:30:31
updated: 2017-03-12 15:30:31
tags: [Github,Coding]
categories: Hexo
description: "上一篇博客介绍了怎么搭建本地的静态博客，本文内容是将本地的博客通过GitHub Pages发布，使外网可以访问,coding上同理，本文只介绍Github。"
---


<!-- # Hexo：Github上发布博客（二）-->

## 前言 ##
上一篇博客介绍了怎么搭建本地的静态博客，本文内容是将本地的博客通过GitHub Pages发布，使外网可以访问,coding上同理，本文只介绍Github。
## 正文 ##
### 准备 ###
发布前的准备：
 - 注册Github账号

### 创建Github 仓库 ###
注册完成之后,点击头像左侧的`+`号, —>`New repository`，创建新仓库
![](Hexo2RemoteRepositoryHostingBlog/1.jpg)

因为创建的是个人网站，所以仓库的名称需要安装GitHub个人网站项目的规定来写。
规则就是：

    username.github.io

比如我的GitHub用户名是`yibierusi`，那我就要填写 `yibierusi.github.io`。然后选择`Public`模式，接着点击`创建仓库`按钮,因为我的已经创建了，所以这里会提示已经存在。
![](Hexo2RemoteRepositoryHostingBlog/2.jpg)

### 推送部署 ###
在根目录下的`_config.yml`中添加git信息，之后在cmd下通过 `hexo d` 一键推送本地代码并部署。
单个仓库配置：

    deploy:
      type: git
      repository: git@github.com:yibierusi/yibierusi.github.io.git
      branch: master
多个仓库配置：

    deploy:
      type: git
      repo:
          github: git@github.com:yibierusi/yibierusi.github.io.git,master
          coding: git@git.coding.net:zhouhy1205/blog.git,master

### 绑定自定义域名 ###
代码推送成功后，进入新仓库，点击`Settings`，找到`GitHub Pages` 设置主题，并且绑定自定义域名
![](Hexo2RemoteRepositoryHostingBlog/3.jpg)
第一个红框是选择分支
第二个红框是选择主题
第三个红框是自定义域名，没有就访问`username.github.io`，如果有域名需要在域名供应上处解析，并设置第三个红框，否则404，我用的是阿里云的域名。解析配置如下图：
![](Hexo2RemoteRepositoryHostingBlog/4.png)

绑定自定义域名后会生成一个`CNAME`文件，文件内容就是刚刚绑定的域名`zhouhy.top`。到了这一步，我们已经完成了个人博客的创建，稍等一两分钟就可以通过域名访问自己的博客了。

## 总结 ##
本文只介绍了`github`托管，但是最好在`coding`上同时托管一份代码，步骤同`github`一样，因为后面会涉及到百度收录网站的问题，`github`是禁止百度爬取的，所以需要走`coding`的路线。