---
title: Hexo系列（三）：上传资源到博客
date: 2017-04-01 09:18:54
updated: 2017-04-01 09:18:54
tags: [Upload,Resources]
categories: Hexo
description: "用Hexo+Github搭建博客的初衷就是不想去购买服务器，备案，搭建服务等，所以也不存在文件上传的问题。那么博客中有时需要用到的图片，这里的图片就是静态图片。"
---

## 前言 ##
用Hexo+Github搭建博客的初衷就是不想去购买服务器，备案，搭建服务等，所以也不存在文件上传的问题。那么博客中有时需要用到的图片，这里的图片就是静态图片。
### 修改配置 ###
cd到博客根目录下 查看`_config.yml`文件 查找 `post_asset_folder` 字段，将post_asset_folder 设置为true ， `post_asset_folder:true`。

当设置 `post_asset_folder` 参数为`true`，在建立文件时，Hexo 会自动建立一个与文章同名的文件夹，可以把与该文章相关的所有资源都放到此文件夹内，这样就可以更方便的使用资源。

### 插件安装 ###
到博客的根目录下执行 `npm install https://github.com/CodeFalling/hexo-asset-image --save` 命令来进行插件的安装。

### 创建文章 ###
然后创建一文章 `hexo new "test"` 然后查看博客的 ../source/_posts 目录下的文件，会看到存在一个test 文件夹 和 test.md 文件
![](Hexo3UploadingResourcesBlog/1.jpg)

### 添加资源 ###
将所需要的图片资源放到test 文件夹 内 目录结构如下：
![](Hexo3UploadingResourcesBlog/2.jpg)

### 使用资源 ###
书写文章使用test文件内 的图片
![](Hexo3UploadingResourcesBlog/3.jpg)

### 效果 ###
1.使用`hexo s` 命令运行本地博客即可看到效果

2.使用`hexo clean` `，hexo g` ，`hexo d`将本地博客推送到远程，即可看到文章中的图片