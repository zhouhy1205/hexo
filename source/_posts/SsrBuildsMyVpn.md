---
title: SSR搭建属于自己的梯子
date: 2018-07-12 16:33:54
updated: 2017-08-08 16:33:54
tags: [Ssr,Vps]
categories: VPN
description: "这几天一直有小伙伴问我有没有免费的梯子，我之前一直在用google的蓝灯，但是后来因为国情原因时好时坏的所以决定自己研究一下梯子的搭建。"
---


## 前言 ##
SSR梯子切勿用于非法用途，主要是用来学习交流使用。
这几天一直有小伙伴问我有没有免费的梯子，我之前一直在用google的蓝灯，但是后来因为国情原因时好时坏的所以决定自己研究一下梯子的搭建。

## 正文 ##
### 准备 ###
SSTAP：链接: https://pan.baidu.com/s/1tfuR5gsUnmTNxurvn_fFNA  密码:67zb
Windows客户端代理工具：链接: https://pan.baidu.com/s/1pqtpMnrHRk89lWDe-IOmFQ 密码:fqkt
服务器一台：推荐使用vultr地址：http://www.vuvps.com/vultr.php
最后一个xShell远程终端，用来连接你的服务器，链接: https://pan.baidu.com/s/1ab2GYTH7YyLCl0qfAxp5Hg  密码:meeu ，只需要用一次就行了，就是SSR搭建后之后，你就可以把它卸载了。
准备支付宝。

### 服务器选购 ###
1.进入到Vultr首页，填写你的邮箱地址和登陆密码（密码需要数字+大写+小写字母），然后点击“CreateAccount”注册账号，注册成功之后,Service一栏里面，需要验证一下自己的邮箱。
![](SsrBuildsMyVpn/1.png)

2.购买服务器需要最低充值10美金，使用支付宝付款，服务器按小时计费。

充值流程：【Billing】-【Alipay】-【Pay withAlipay】-【完成支付】
![](SsrBuildsMyVpn/2.png)

3.支付完成后，接着选择服务器，本着最便宜的原则来购买（一般纽约为2.5美元一个月）

【Server Location】
![](SsrBuildsMyVpn/3.png)
【ServerType】 选择Centos 6x64（默认是Centos 7，如果没选6也没关系）
![](SsrBuildsMyVpn/4.png)
【ServerSize】反正选最便宜的。
![](SsrBuildsMyVpn/5.png)
剩下的默认，最后点击右下角“DeployNow”完成购买。

PS:主机创建的时候需要等两分钟再执行下一步SSR部署

### SSR部署 ###

1.下载xShell,上面有链接，vps信息在你刚刚购买的面板里面可以看到。

打开xshell，新建链接，填写主机ip，端口号，钩选重新链接，然后点连接。
![](SsrBuildsMyVpn/6.png)
弹出“SSR安全警告”，选择“接受并保存”，用户名root（记得钩选记住用户名），密码在你的vultr里面去看，
![](SsrBuildsMyVpn/7.png)
出现下图（root@vultr）代表成功
![](SsrBuildsMyVpn/8.png)
### ~~SSR安装(已过期)~~ ###

执行以下命令（密码建议修改，port可以默认）

    wget --no-check-certificate https://freed.ga/github/shadowsocksR.sh; bash shadowsocksR.sh



![](SsrBuildsMyVpn/9.png)
![](SsrBuildsMyVpn/10.png)

出现提示“Shadowsocksr安装完成”就可以继续下一步，锐速安装（图片保存一下）。

#### 锐速安装

因为 Vultr 的所有机房都位于国外，当晚上上网高峰期来临时，在连接速度上会比较慢，所以我们有必要安装一些程序来加速连接速度。本次推荐安装的是站长一直在用的锐速加速软件，个人认为目前在提速方面，相比于最新的 Google BBR 拥塞控制算法，锐速尚有优势。

**1.执行更换内核脚本**

    wget -N --no-check-certificate https://freed.ga/kernel/ruisu.sh&& bash ruisu.sh

执行之后，脚本会自动断开服务器，假如你钩选了记住用户名和密码，它会自动重连，如果没钩选也没关系，Xshel左上角【File】-【重新连接】，然后执行锐速安装脚本。

**2.锐速安装脚本**

**注意事项**

    1、安装锐速需降级系统内核，而安装 Google BBR 则需升级系统内核，故两者不能同时安装。
    
    2、安装锐速需降级系统内核，有可能造成系统不稳定，故不建议将其应用在重要的生产环境中。
    
    3、本教程只支持 CentOS6 x64 及 CentOS7 x64 系统，不支持任何 Debian & Ubuntu 系统！

输入`uname -r`查看当前系统内核版本。主要分三种情况：

**1、结果以 2 开头，例如 2.6.32-696.18.7.el6.x86_64。**

    wget --no-check-certificate -O appex.sh https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh && bash appex.sh install '2.6.32-642.el6.x86_64'


2、结果以 3 开头，例如 3.10.0-693.11.6.el7.x86_64。

    yum install net-tools -y && wget --no-check-certificate -O appex.sh https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh && bash appex.sh install。

3、结果以 4 开头，例如 4.12.10-1.el7.elrepo.x86_64。

这种输出结果说明我们的服务器已经安装 Google BBR 拥塞控制算法，此时已经无法继续安装锐速。

如果出现版本不匹配，会出现一个选项，直接选个1就行了，直至出现下图，恭喜你，拥有了一个属于自己的SSR梯子。
![](SsrBuildsMyVpn/11.png)



#### 常用命令

```
启动 ShadowsocksR：
/etc/init.d/ssr start
停止 ShadowsocksR：
/etc/init.d/ssr stop
重启 ShadowsocksR：
/etc/init.d/ssr restart
查看 ShadowsocksR状态：
/etc/init.d/ssr status
```

或者

```
python /usr/local/shadowsocks/shadowsocks/server.py -c /etc/shadowsocks.json -d start/stop/restart/status
```



### SSR安装(最新)

```
wget -N --no-check-certificate https://gitee.com/zhouhy1205/SSR/raw/master/ssrmu.sh && chmod +x ssrmu.sh && bash ssrmu.sh
```

**注意**：

```
协议：http_simple

认证方式：auth_sha1_v4

加密方式：aes-256-cfb
```



另外还可以直接用SSH命令实现

```
ssh -qTfnN -D 8080 username@your.remote.ssh.server
```



**安装完后可以在其他安装中选择安装谷歌的BBR或者锐速 进行加速。也可以在github上下载最新的[BBR](https://github.com/google/bbr/)或者其他加速**





### SStap配置 ###

下载SStap,安装并填写SSR信息，下载链接在上面。

你只需要填写服务器IP、端口号、密码，其他信息照着下图抄就行了,此处注意SStap的版本，因为某些原因高版本的SStap屏蔽了网站的加速。
![](SsrBuildsMyVpn/12.png)
最后给个Google一张
![](SsrBuildsMyVpn/13.png)
SSR各平台下载地址=》 [极光SSR各平台客户端操作软件](http://www.vuvps.com/?p=112)