---
title: Linux常用软件
date: 2020-03-16 14:27:30
updated: 2020-03-16 14:27:30
tags: [App]
categories: Linux
description: "Linux的软件生态跟Windows完全没有可比性，但是他免费、高安全性、高可定制化、高性能等优点依然吸引着一大批的人。由于生态原因很多在Windowsh很常见随便一搜索一大堆的情况在Linux上就不适用了，所以在这里记录一下一些常用的软件或者很实用但流传度不广的。"
---



## 前言

Linux的软件生态跟Windows完全没有可比性，但是他免费、高安全性、高可定制化、高性能等优点依然吸引着一大批的人。由于生态原因很多在Windowsh很常见随便一搜索一大堆的情况在Linux上就不适用了，所以在这里记录一下一些常用的软件或者很实用但流传度不广的。



## 正文

### 浏览器

**谷歌浏览器**

不多做介绍。



**Chromium**

Chromium同样是一款Chrome浏览器驱动引擎，其目的是为了创建一个安全、稳定和快速的浏览器。它采用Google独家开发出的V8引擎以提升JavaScript的效率，而且还设计了沙盒、黑名单、无痕浏览等功能来实现稳定与安全的网页浏览环境。



### 文本编辑器

**Atom**

Atom是一个在线文本编辑器，它具有简洁和直观的图形界面，还支持CSS，HTML，JavaScript等网页编程语言，同时具有宏自动完成分屏、集成文件管理器等功能。



### Markdown编辑器

**Typora**

Typora是一款markdown编辑器，它将写作界面和预览界面二合为一，插入表格像在word中一样简单，支持直接拖拽插入图片，支持代码和数学公式输入，可以显示和隐藏大纲，自定义主题。本文就是用**Typora**写的。



**Cmd Markdown**

Cmd Markdown是一款MarkDown编辑器，同时也是一个阅读工具，支持实时同步预览，区分写作和阅读模式，支持在线存储和分享文稿网址。可以编辑工具栏，可以实时保存数据到云端，支持离线模式，支持标签/分类/搜索等操作。

### 数据库可视化工具

**DataGrip**

jetbrains出品。



**Navicat**

同样不多做介绍。



**DBeaver**

DBeaver是一个通用的数据库管理工具和SQL客户端，支持 MySQL、PostgreSQL、Oracle、 DB2、MSSQL、 Sybase、 Mime、 HSQLDB、Derby以及其他兼容 JDBC 的数据库。DBeaver 提供一个图形界面用来查看数据库结构、执行SQL查询和脚本，浏览和导出数据，处理BLOB/CLOB数据，修改数据库结构等。



### API调试

**ApiPost**

ApiPost是一个支持团队协作，并可直接生成文档的API调试、管理工具 支持模拟POST、GET、PUT等常见请求，是后台接口开发者或前端、接口测试人员不可多得的工具



**Postman**

大名鼎鼎的Postman在Windows上很好用，在Liunx上同样不错，但是为什么会在ApiPost后面呢 ！emmmmm



### Python 虚拟环境管理

- 进行全局的 Python 版本切换
- 为单个项目提供对应的 Python 版本
- 使用环境变量能让你覆盖 Python 版本
- 能在同一时间在不同版本的 Python 间进行命令搜索

以下三大工具都有以上特点。



**Pyenv**

可以让你轻松地在多个版本的 Python 之间切换。它简单而优雅，目的单一。

**pyvenv**

Python 从3.3 版本开始，自带了一个虚拟环境 [venv](https://docs.python.org/3/library/venv.html)，在 [PEP-405](http://legacy.python.org/dev/peps/pep-0405/) 中可以看到它的详细介绍。它的很多操作都和 virtualenv 类似。

因为是从 3.3 版本开始自带的，这个工具也仅仅支持 python 3.3 和以后版本。所以，要在 python2 上使用虚拟环境，依然要利用 [virtualenv](http://www.virtualenv.org/) 。

**Virtualenv**

pip, virtualenv, fabric通称为pythoner的三大神器。[virtualenv](http://www.virtualenv.org/) 是目前最流行的 python 虚拟环境配置工具。它不仅同时支持 python2 和 python3，而且可以为每个虚拟环境指定 python 解释器，并选择不继承基础版本的包。



### 内网穿透

我记得好像之前写过一篇Windows上的各种内网穿透工具优缺点对比，其中cpolar非常突出，那么它有Linux版吗？



**cpolar**

安全的内网穿透， 只需一行命令，就可以将内网站点穿越任意NAT防火墙暴露至公网，方便给客户演示。高效调试微信公众号、小程序、对接支付宝网关等云端服务，提高您的编程效率。



### **远程工具**

**electerm**

electerm是一个终端、文件管理器、SSH/SFTP客户端, 基于electron/ssh2/node-pty/xterm/antd等组件。能够保存主机地址、用户名、密码、端口号等信息，并支持使用密码和私钥进行登录。



**deepin-terminal**

深度终端是深度开发的一款终端管理器，可以按照用户的要求对窗口进行任意分割，支持水平和垂直屏幕分割，允许用户改变字体、颜色和自定义背景图片、调整透明度等终端设置。您可以通过终端窗口使用键盘输入各种操作命令等。**上传下载需要安装rz，sz**。



**Remmina**

Remmina是一个远程桌面客户端，它提供了RDP、VNC、XDMCP、SSH等远程连接协议的支持。其优点在于界面清爽，方便易用。



**FinalShell**

FinalShell是一体化的服务器、网络管理软件。它不仅是SSH客户端，还是功能强大的开发运维工具，充分满足开发运维需求。支持海外服务器远程桌面加速，SSH加速，双边TCP加速，内网穿透及其他实用功能。**功能比较齐全，可以对服务器性能，进程进行监控。但是比较耗性能。**



 **GSTM**（Gnome SSH Tunnel Manager）

**非远程工具**，只支持**SSH隧道**



### 系统工具

**BleachBit**

**垃圾清理工具**，BleachBit是一款开源的系统清理工具，它可以释放磁盘空间，保护您的隐私，清除缓存，删除cookies、internet历史、临时文件、日志和丢弃的垃圾文件等，支持清除应用的残留数据，切碎文件防止恢复、隐藏被删除文件的痕迹等功能。



**GParted**

**分区工具**，GParted是一款Linux下的功能非常强大的分区工具，GParted可以创建、删除分区，也可以调整分区的大小和移动分区的位置。



**Disks**

硬盘挂载非常方便，Disks是一款磁盘管理工具，它可以读取硬盘、光驱、U盘等信息，同时还具有查看硬盘详细信息、格式化硬盘、编辑分区类型、创建镜像文件等功能。



**Baobab**

**磁盘分析工具**，Baobab是一款分析磁盘使用情况的图形工具，它可以分析本地硬盘空间、分析挂载硬盘或设备空间，同时界面简单易操作，且分析结果以环状或树形状显示。



**Grub-Customizer**

**启动项编辑器**，Grub Customizer 是GRUB2的图形化管理程序。目前，它只提供GRUB2菜单选项编辑功能：重新排序、重命名或增加/删除选项。Grub Customizer会改变boot.cfg文件，所以如果你运行”sudo update-grub”，Grub Customizer作出的改变也不会被重置。



### Redis

**RedisPlus**

RedisPlus是为Redis可视化管理开发的一款开源免费的桌面客户端软件，支持Windows 、Linux、Mac三大系统平台，RedisPlus提供更加高效、方便、快捷的使用体验，有着更加现代化的用户界面风格。该软件支持单机、集群模式连接，同时还支持SSH（单机、集群）通道连接。



### 服务器管理工具

**宝塔Linux面板**

宝塔Linux面板是一款服务器管理工具，自带环境包，可一键包装nginx、apache、php、mysql、pureftpd、phpmyadmin等环境，自带在线网页管理面板，可用于建站、开通FTP、防火墙管理、以及强大的在线文件管理功能等。



### VPN

**Shadowsocks-Qt5**

看到这个单词Shadowsocks你就明白了，Shadowsocks-Qt5是一款Shadowsocks客户端，可以让你的浏览器突破地区限制，自由访问互联网。Shadowsocks-Qt5支持流量统计，支持服务器延迟测试，还可以同时使用多个配置文件。



### 下载器

**qBittorrent**

qBittorrent是一个轻量级BitTorrent客户端，它支持文件上传/下载、支持DHT网络、数据交换、文件选择性下载、预览媒体文件、支持Unicode、支持代理连接、远程控制等功能。



**Persepolis Download Manager**

Persepolis Download Manager是一款即开即用的 aria2 客户端，用来下载网络上的文件，支持 HTTP / HTTPS，FTP，SFTP，BitTorrent 和 Metalink 等协议。aria2 不占用系统资源，支持多线程下载、多协议支持、BT 客户端，支持远程访问。



**Deluge**

Deluge是一个BitTorrent客户端软件，它支持插件管理、种子交换、下载限速以及多个线程同时下载、文件预览、选择下载目录、队列管理资源等功能。


