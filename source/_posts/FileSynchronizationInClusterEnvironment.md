---
title: 集群环境下的文件同步
date: 2018-07-12  15:43:29
updated: 2018-07-12  15:43:29
tags: [Cluster,Scp,Sftp]
categories: Java
description: "最近公司的项目，需要做一个上传下载的功能，正准备把大学写的demo拿来用一用，然后反应过来，我们项目部署的是集群。因为使用nginx均衡负载，由于分发请求到不同服务器，这样如果只想把上传的图片只保存到一台服务器，会涉及到多服务器文件同步的问题。"
---

## 前言 ##
最近公司的项目，需要做一个上传下载的功能，正准备把大学写的demo拿来用一用，然后反应过来，我们项目部署的是集群。因为使用nginx均衡负载，由于分发请求到不同服务器，这样如果只想把上传的图片只保存到一台服务器，会涉及到多服务器文件同步的问题。

例如：我上传文件时，负载到A服务器，文件上传到A服务器下路径，我下载时被分配到B服务器了，此时B服务器相应的路径下并没有我在A上传的文件。

那么，如何解决集群环境文件上传不同步的问题呢？

## 解决方案 ##

 1. 文件保存到数据库，目前数据库单个字段存储大小可以高达几个GB，所以存储到数据库完全没问题，至于访问速度以及其他问题，emmmmm（实用性太低）。
 2. 宿主机的nginx对于上传的location做特殊处理, 只让一台机器来负责文件上传, 不完美但是有效, 可以作为一段时间的过度方案。（治标不治本）
 3. 使用SFTP、SCP、xshell同步文件到文件服务器，或者多个负载服务器上。
 4. 搭建一个分布式文件系统（或者硬盘）, 挂载到所有的服务器上，使被挂载的成为一个局域网网络盘, 处理文件上传后写只写入这个特定目录即可，不需要考虑同步问题。（后面有时间出一下Linux挂载的文章）
 5. 使用阿里巴巴的OSS服务。（费钱）


## 使用SCP操作远程服务器 ##
### SCP ###
scp是secure copy的简写，用于在Linux下进行远程拷贝文件的命令，和它类似的命令有cp，不过cp只是在本机进行拷贝不能跨服务器，而且scp传输是加密的。可能会稍微影响一下速度。当你服务器硬盘变为只读 read only system时，用scp可以帮你把文件移出来。另外，scp还非常不占资源，不会提高多少系统负荷，在这一点上，rsync就远远不及它了。虽然 rsync比scp会快一点，但当小文件众多的情况下，rsync会导致硬盘I/O非常高，而scp基本不影响系统正常使用。

    scp root@192.168.120.204:/opt/soft/nginx-0.5.38.tar.gz /opt/soft/
贴一个使用指令，具体其他参数，可以google。
#### 依赖 ####
使用Java代码执行scp需要使用ganymed-ssh2包，这个包是十几年前的，不过能用就行。

    <dependency>
        <groupId>ch.ethz.ganymed</groupId>
        <artifactId>ganymed-ssh2</artifactId>
        <version>build210</version>
    </dependency>


#### 具体实现 ####

    import ch.ethz.ssh2.Connection;
    import ch.ethz.ssh2.SCPClient;
    import ch.ethz.ssh2.SFTPv3Client;
    import ch.ethz.ssh2.SFTPv3DirectoryEntry;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    
    import java.io.File;
    import java.io.IOException;
    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;
    import java.util.Vector;
    
    /**
     * 文件帮助类
     */
    public class SSHUtil {
        private static Logger logger = LoggerFactory.getLogger(SSHUtil.class);
    
        private String IP = "10.128.118.188";
        private int PORT = 22;
        private String USER = "was";//登录用户名
        private String PASSWORD = "<gF3>gWf0D";//生成私钥的密码和登录密码，这两个共用这个密码
        private Connection connection = null;
    
        private String PRIVATEKEY = "C:\\Users\\ubuntu\\.ssh\\id_rsa";// 本机的私钥文件
        private boolean usePassword = true;// 使用用户名和密码来进行登录验证。如果为true则通过用户名和密码登录，false则使用rsa免密码登录
    
        public SSHUtil(String IP, int PORT, String USER, String PASSWORD) {
            this.IP = IP;
            this.PORT = PORT;
            this.USER = USER;
            this.PASSWORD = PASSWORD;
            this.usePassword = true;
            this.connection = new Connection(IP, PORT);
        }
    
        /**
         * ssh用户登录验证，使用用户名和密码来认证
         *
         * @param user
         * @param password
         * @return
         */
        public boolean isAuthedWithPassword(String user, String password) {
            try {
                return connection.authenticateWithPassword(user, password);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
    
        /**
         * ssh用户登录验证，使用用户名、私钥、密码来认证 其中密码如果没有可以为null，生成私钥的时候如果没有输入密码，则密码参数为null
         *
         * @param user
         * @param privateKey
         * @param password
         * @return
         */
        public boolean isAuthedWithPublicKey(String user, File privateKey, String password) {
            try {
                return connection.authenticateWithPublicKey(user, privateKey, password);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
    
        /**
         * 认证
         * @return
         */
        public boolean isAuth() {
            if (usePassword) {
                return isAuthedWithPassword(USER, PASSWORD);
            } else {
                return isAuthedWithPublicKey(USER, new File(PRIVATEKEY), PASSWORD);
            }
        }
    
        /**
         * 从远程服务器下载文件
         * @param remoteFile /opt/1.zip
         * @param path  /opt
         */
        public void getFile(String remoteFile, String path) {
            try {
                connection.connect();
                boolean isAuthed = isAuth();
                if (isAuthed) {
                    System.out.println("认证成功!");
                    SCPClient scpClient = connection.createSCPClient();
                    scpClient.get(remoteFile, path);
                } else {
                    System.out.println("认证失败!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.close();
            }
        }
    
        /**
         * 在远端linux上创建文件夹
         *
         * @param dirName          文件夹名称
         * @param posixPermissions 目录或者文件夹的权限
         */
        public void mkDir(String dirName, int posixPermissions) {
            try {
                connection.connect();
                boolean isAuthed = isAuth();
                if (isAuthed) {
                    SFTPv3Client sftpClient = new SFTPv3Client(connection);
                    List<String> dirNames = new ArrayList<>(Arrays.asList(dirName.split("/", -1)));
                    dirNames.remove("");
                    dirNames.remove("");
                    String path = "";
                    for (int i = 0; i < dirNames.size() - 1; i++) {
                        path += "/" + dirNames.get(i);
                        Vector ls = sftpClient.ls(path);
                        String nextDirName = dirNames.get(i + 1);
                        if (isContainsDirectoryByName(ls, nextDirName)) {
                            continue;
                        } else {
                            sftpClient.mkdir(path + "/" + dirNames.get(i + 1), posixPermissions);
                        }
                    }
                } else {
                    System.out.println("认证失败!");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }
    
        /**
         * 向远程服务器推文件
         * @param localFile 本地文件路径 例如：/opt/1.zip
         * @param remoteTargetDirectory 远程服务器路径 例如：/opt
         */
        public void putFile(String localFile, String remoteTargetDirectory) {
            try {
                connection.connect();
                boolean isAuthed = isAuth();
                if (isAuthed) {
                    SCPClient scpClient = connection.createSCPClient();
                    //创建目录
                    SFTPv3Client sftpClient = new SFTPv3Client(connection);
                    List<String> dirNames = new ArrayList<>(Arrays.asList(remoteTargetDirectory.split("/", -1)));
                    //移除首尾带/
                    dirNames.remove("");
                    dirNames.remove("");
                    String path = "";
                    //逐级创建目录
                    for (int i = 0; i < dirNames.size() - 1; i++) {
                        path += "/" + dirNames.get(i);
                        Vector ls = sftpClient.ls(path);
                        String nextDirName = dirNames.get(i + 1);
                        if (isContainsDirectoryByName(ls, nextDirName)) {
                            continue;
                        } else {
                            sftpClient.mkdir(path + "/" + dirNames.get(i + 1), 0755);
                        }
                    }
                    scpClient.put(localFile, remoteTargetDirectory);
                } else {
                    System.out.println("认证失败!");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }
    
        /**
         * 是否包含文件夹名为XXX的
         *
         * @return
         */
        public boolean isContainsDirectoryByName(Vector<SFTPv3DirectoryEntry> dirs, String dirName) {
            if (dirs != null && dirs.size() != 0) {
                for (int i = 0; i < dirs.size(); i++) {
                    SFTPv3DirectoryEntry sde = dirs.get(i);
                    if (sde.attributes.isDirectory() && sde.filename.equals(dirName)) {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }
    
    }

代码主要是传文件部分，因为jar包过于古老且停止更新了，其余方法懒得写了，感兴趣的可以阅读源码。
### SFTP ###
#### 依赖 ####
        <dependency>
            <groupId>commons-net</groupId>
            <artifactId>commons-net</artifactId>
            <version>3.6</version>
        </dependency>

#### 具体代码 ####

    /** 
    	 * Description: 向FTP服务器上传文件 
    	 * @param host FTP服务器hostname 
    	 * @param port FTP服务器端口 
    	 * @param username FTP登录账号 
    	 * @param password FTP登录密码 
    	 * @param basePath FTP服务器基础目录
    	 * @param filePath FTP服务器文件存放路径。文件的路径为basePath+filePath
    	 * @param filename 上传到FTP服务器上的文件名 
    	 * @param input 输入流 
    	 * @return 成功返回true，否则返回false 
    	 */  
    	public static boolean uploadFile(String host, int port, String username, String password, String basePath,
    			String filePath, String filename, InputStream input) {
    		boolean result = false;
    		FTPClient ftp = new FTPClient();
    		try {
    			int reply;
    			ftp.connect(host, port);// 连接FTP服务器
    			// 如果采用默认端口，可以使用ftp.connect(host)的方式直接连接FTP服务器
    			ftp.login(username, password);// 登录
    			reply = ftp.getReplyCode();
    			if (!FTPReply.isPositiveCompletion(reply)) {
    				ftp.disconnect();
    				return result;
    			}
    			//切换到上传目录
    			if (!ftp.changeWorkingDirectory(basePath+filePath)) {
    				//如果目录不存在创建目录
    				String[] dirs = filePath.split("/");
    				String tempPath = basePath;
    				for (String dir : dirs) {
    					if (null == dir || "".equals(dir)) continue;
    					tempPath += "/" + dir;
    					if (!ftp.changeWorkingDirectory(tempPath)) {
    						if (!ftp.makeDirectory(tempPath)) {
    							return result;
    						} else {
    							ftp.changeWorkingDirectory(tempPath);
    						}
    					}
    				}
    			}
    			//设置上传文件的类型为二进制类型
    			ftp.setFileType(FTP.BINARY_FILE_TYPE);
    			//上传文件
    			if (!ftp.storeFile(filename, input)) {
    				return result;
    			}
    			input.close();
    			ftp.logout();
    			result = true;
    		} catch (IOException e) {
    			e.printStackTrace();
    		} finally {
    			if (ftp.isConnected()) {
    				try {
    					ftp.disconnect();
    				} catch (IOException ioe) {
    				}
    			}
    		}
    		return result;
 		}

## 总结 ##
方法都是大同小异，知道原理即可，其余的无外乎用什么工具，君子性非异也，善假于物。