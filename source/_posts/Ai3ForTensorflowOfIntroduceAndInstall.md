---
title: 人工智能(三)：TensorFlow的介绍与安装
date: 2019-08-27 20:11:28
updated: 2019-08-27 20:11:28
tags: [Tensorflow,DeepLearning]
categories: DL
description: "Tensorflow是目前最火的深度学习框架，广泛应用于自然语言处理、语音识别、图像处理等多个领域。不仅深受全球深度学习爱好者的广泛欢迎，Google、eBay、Uber、OPenAI等众多科技公司的研发团队也都在使用它。"
---
## Tensorflow 的介绍 ##
Tensorflow是一个基于数据流编程（dataflow programming）的符号数学系统，被广泛应用于各类机器学习（machine learning）算法的编程实现，其前身是谷歌的神经网络算法库DistBelief [1]  。

Tensorflow是目前最火的深度学习框架，广泛应用于自然语言处理、语音识别、图像处理等多个领域。不仅深受全球深度学习爱好者的广泛欢迎，Google、eBay、Uber、OPenAI等众多科技公司的研发团队也都在使用它。

Tensorflow是由谷歌大脑团队于2015年11月开发的第二代开源的机器学习系统。Tensorflow支持python、C++、java、GO等多种编程语言，以及CNN、RNN和GAN等深度学习算法。Tensorflow除可以在Windows、Linux、MacOS等操作系统运行外，还支持Android和iOS移动平台的运行、树莓派等开发板中、以及适用于多个CPU/GPU组成的分布式系统中。

相较于其它的深度学习框架，如：Caffe、Torch、Keras、MXnet、Theano等，Tensorflow的主要优势有以下几点：高度的灵活性、支持python语言开发、可视化效果好、功能更加强大、运行效率高、强大的社区。

## Tensorflow安装与配置 ##

目前，Windows、Linux和MacOS均已支持Tensorflow。文章将以Windows系统的安装为例。

在安装Tensorflow前，我们要先安装Anaconda，因为它集成了很多Python的第三方库及其依赖项，方便我们在编程中直接调用。

Anaconda下载地址为：https://www.anaconda.com/download/。

下载好安装包后，一步步next执行安装过程。


安装好Anaconda后，可以打开命令提示符，输入`pip install Tensorflow`完成Tensorflow的安装。
此处默认安装为cpu版，如果需要安装gpu版 需要执行`pip install Tensorflow-gpu`，**注意 gpu版安装后需要安装对应gpu版本的驱动才能使用gpu进行训练**。
之后我们进入python可执行界面，输入`import tensorflow as tf`来检验Tensorflow是否安装成功。如果没有报任何错，可以正常执行，则说明Tensorflow已经安装成功。

### HOME ###
![](Ai3ForTensorflowOfIntroduceAndInstall/anaconda.png)
home中包含了一些已经默认安装好的工具和一些推荐使用的工具可以一键安装。
**Jupyter Notebook**是一款非常好用的交互式开发工具，不仅支持40多种编程语言，还可以实时运行代码、共享文档、数据可视化、支持markdown等，适用于机器学习、统计建模数据处理、特征提取等多个领域。尤其在Kaggle、天池等数据科学竞赛中，快捷、实时、方便的优点深受用户欢迎。本书后边的章节中，均将以Jupyter Notebook作为开发环境，运行Tensorflow程序。

### Environments ###
![](Ai3ForTensorflowOfIntroduceAndInstall/environments.png)
环境中包含了已经安装的、未安装的、需要更新的、所有的环境。例如Tensorflow。

其他的就不多做介绍，都是一看就懂的东西，复杂点的可以在使用过程中慢慢摸索。到这Tensorflow就安装完成了。

## 计算图模型 ##
### Tensor ###
![](Ai3ForTensorflowOfIntroduceAndInstall/intro_tensorflow_01.png)
**Tensor** :张量 ，在数学上，张量是N维向量，这意味着张量可以用来表示N维数据集。上面的图有点复杂，难以理解。我们看看它的简化版本：
![](Ai3ForTensorflowOfIntroduceAndInstall/intro_tensorflow_02.png)
上图显示了一些简化的张量。随着维度的不断增加，数据表示将变得越来越复杂。例如，一个3x3的张量，我可以简单地称它为3行和3列的矩阵。如果我选择另一个形式的张量（1000x3x3），我可以称之为一个向量或一组1000个3x3的矩阵。在这里我们将（1000x3x3）称为张量的形状或尺寸。张量可以是常数也可以是变量。



### Flow ###
**Flow**:计算图，流。流是指一个计算图或简单的一个图，图不能形成环路，图中的每个节点代表一个操作，如加法、减法等。每个操作都会导致新的张量形成。
![](Ai3ForTensorflowOfIntroduceAndInstall/intro_tensorflow_03.png)

上图展示了一个简单的计算图，所对应的表达式为：

    e = (a+b)x(b+1)
计算图具有以下属性：

 - 叶子顶点或起始顶点始终是张量。意即，操作永远不会发生在图的开头，由此我们可以推断图中的每个操作都应该接受一个张量并产生一个新的张量。同样，张量不能作为非叶子节点出现，这意味着它们应始终作为输入提供给操作/节点。
 - 计算图总是以层次顺序表达复杂的操作。通过将a + b表示为c，将b + 1表示为d，可以分层次组织上述表达式。 因此，我们可以将e写为：

    `e = c*d   ##这里 c = a+b 且 d = b+1.`

 - 以反序遍历图形而形成子表达式，这些子表达式组合起来形成最终表达式。

 - 当我们正向遍历时，遇到的顶点总是成为下一个顶点的依赖关系，例如没有a和b就无法获得c，同样的，如果不解决c和d则无法获得e。

 - **同级节点的操作彼此独立，这是计算图的重要属性之一**。当我们按照图中所示的方式构造一个图时，很自然的是，在同一级中的节点，例如c和d，彼此独立，这意味着没有必要在计算d之前计算c。
   因此它们可以并行执行。

### 计算图模型 ###
Tensorflow是一种计算图模型，即用图的形式来表示运算过程的一种模型。Tensorflow程序一般分为图的构建和图的执行两个阶段。图的构建阶段也称为图的定义阶段，该过程会在图模型中定义所需的运算，每次运算的的结果以及原始的输入数据都可称为一个节点（operation ，缩写为op）。我们通过以下程序来说明图的构建过程：
程序1：
![](Ai3ForTensorflowOfIntroduceAndInstall/c1.png)

程序1定义了图的构建过程，“import tensorflow as tf”，是在python中导入tensorflow模块,并另起名为“tf”；接着定义了两个常量op，m1和m2，均为1*2的矩阵；最后将m1和m2的值作为输入创建一个矩阵加法op，并输出最后的结果result。

**Add_1:0**:图的名字。
**shape(2,)**:相当于shape(2,1),代表图的形状2列一行。
**dtype=int32**:图的类型，int32。


我们分析最终的输出结果可知，其并没有输出矩阵相加的结果，而是输出了一个包含三个属性的Tensor。

以上过程便是图模型的**构建阶段**：**只在图中定义所需要的运算，而没有去执行运算。**我们可以用下图来表示：
![](Ai3ForTensorflowOfIntroduceAndInstall/c1.png)


第二个阶段为图的执行阶段，也就是在会话（session）中执行图模型中定义好的运算。
我们通过程序2来解释图的执行阶段：

程序2：
![](Ai3ForTensorflowOfIntroduceAndInstall/c2.png)


程序2描述了图的执行过程，首先通过“tf.session()”启动默认图模型，再调用run()方法启动、运行图模型，传入上述参数result，执行矩阵的加法，并打印出相加的结果，最后在任务完成时，要记得调用close()方法，关闭会话。

除了上述的session写法外，我们更建议大家，把session写成如程序3所示“with”代码块的形式，这样就无需显示的调用close释放资源，而是自动地关闭会话。

程序3：
![](Ai3ForTensorflowOfIntroduceAndInstall/c3.png)


此外，我们还可以利用CPU或GPU等计算资源分布式执行图的运算过程。一般我们无需显示的指定计算资源，Tensorflow可以自动地进行识别，如果检测到我们的GPU环境，会优先的利用GPU环境执行我们的程序。但如果我们的计算机中有多于一个可用的GPU，这就需要我们手动的指派GPU去执行特定的op。如下程序4所示，Tensorflow中使用`with...device`语句来指定GPU或CPU资源执行操作。

程序4：
![](Ai3ForTensorflowOfIntroduceAndInstall/c4.png)


上述程序中的“`tf.device(“/gpu:2”)`”是指定了第二个GPU资源来运行下面的op。依次类推，我们还可以通过“/gpu:3”、“/gpu:4”、“/gpu:5”...来指定第N个GPU执行操作。

Tensorflow中还提供了默认会话的机制，如程序5所示，我们通过调用函数`as_default()`生成默认会话。

程序5：
![](Ai3ForTensorflowOfIntroduceAndInstall/c5.png)


我们可以看到程序5和程序2有相同的输出结果。我们在启动默认会话后，可以通过调用`eval()`函数，直接输出变量的内容。

有时，我们需要在Jupyter或IPython等python交互式环境开发。Tensorflow为了满足用户的这一需求，提供了一种专门针对交互式环境开发的方法`InteractiveSession()`,具体用法如程序6所示：

程序6：
![](Ai3ForTensorflowOfIntroduceAndInstall/c6.png)


程序6就是交互式环境中经常会使用的`InteractiveSession()`方法，其创建`sess`对象后，可以直接输出运算结果。


综上所述，我们介绍了Tensorflow的核心概念——计算图模型，以及定义图模型和运行图模型的几种方式。接下来，思考一个问题，为什么Tensorflow要使用图模型？图模型有什么优势呢？

首先，图模型的最大好处是节约系统开销，提高资源的利用率，可以更加高效的进行运算。因为我们在图的执行阶段，只需要运行我们需要的op,这样就大大的**提高了资源的利用率**；其次，这种结构有利于我们提取中间某些节点的结果，方便以后利用中间的节点去进行其它运算；还有就是这种结构对分布式运算更加友好，运算的过程可以分配给多个CPU或是GPU同时进行，提高运算效率；最后，因为图模型把运算分解成了很多个子环节，所以这种结构也让我们的求导变得更加方便。
