---
title: 游戏辅助（二）：基于C的驱动级外设模拟
date: 2017-07-11 17:05:20
updated: 2017-07-11 17:05:20
tags: [DdXoft,Dll,Script]
categories: GameAssist
description: "主要对外设的输入层次、模拟层次的介绍，以及通过基于C的dll插件实现自动执行外设的输入，达到解放双手、提高（外设）输入速度、频率或者高操作的效果。"
---

## 前言 ##
本文主要对外设的输入层次、模拟层次的介绍，以及通过基于C的dll插件实现自动执行外设的输入，达到解放双手、提高（外设）输入速度、频率或者高操作的效果。
### 输入层次介绍 ###

1、当按下键后，键盘或鼠标产生扫描码（不同芯片的键盘产生扫描码不一样）。

2、扫描码被送给相应硬件的驱动，驱动将扫描码转换成虚拟码（就是以  vk_  打头的，其实就是byte大小的数了，不同语言的虚拟码不全一样）。

3、虚拟码插入消息队列，等待被传给相应程序。然后就是程序处理的事了。


### 模拟层次介绍 ###

1、局部模拟：用程序生成虚拟码，将虚拟码直接发给程序。

其一：Java自带的Robot就是这么干的。处理普通程序还有效，游戏就不用想了。

其二：jna的自带platform中，User32类 的sendMessage、postMessage方法。处理普通程序还有效，游戏就不用想了。

2、全局模拟：将扫描码、虚拟码插入到消息队列，然后就不管了。

其一：jna的自带platform中，User32类 的keybd_event方法。处理普通程序还有效，游戏就不用想了。

其二：键盘钩子。没试过，C语言还停留在大学水平，听说改键是这么做的。

3、驱动级模拟：用程序模仿键盘端口给系统发送扫描码。

## 正文 ##
驱动级模拟，本来准备用jna + winio试一下的。后来发现网上不少说winio太出名了，游戏会针对性防范，再加上winio64位版还要签名，所以没试，然后因为种种原因DM插件也没能使用。

后来万能的吧友提供了一个AHK的脚本可以使用，破解后发现底层还是调用的DLL。于是有了本文，用Java调用DDxoft实现驱动级模拟。

### 加载Dll ###

    import com.sun.jna.Library;
    import com.sun.jna.Native;
    
    public interface DdXoft extends Library {
        DdXoft INSTANCE = (DdXoft) Native.loadLibrary(UnifyEnum.DDXOFT64.v(), DdXoft.class);
        //64位JAVA调用*64.dll, 32位调用*32.dll 。与系统本身位数无关。、
        int DD_mov(int x, int y);//绝对移动
        int DD_movR(int dx, int dy);//相对移动
        int DD_btn(int btn);//鼠标
        int DD_whl(int whl); //滑轮
        int DD_key(int ddcode, int flag);//键盘
        int DD_str(String s);//字符串
    }

### 定位 ###
位置判断还是用的Robot

    /**
     * 判断某点颜色是否相同 相同则按下
     *
     * @return
     */
    public static boolean existPressKey(Robot robot, Entry entry, int ddCode) {
        if (isEquals(robot, entry)) {
            return pressKey(robot, entry, ddCode);
        }
        return false;
    }

### 事件 ###
直接调用DDxoft,一些底层的东西还是C靠谱

    /**
     * 直接按下某键
     */
    public static boolean pressKey(Robot robot, Entry entry, int ddCode) {
        //robot.keyPress(keyCode);
        DdXoft.INSTANCE.DD_key(ddCode, 1);
        robot.delay(entry.press);
        //robot.keyRelease(keyCode);
        DdXoft.INSTANCE.DD_key(ddCode, 2);
        robot.delay(entry.release);
        System.out.println("{" + entry.skill + "}" +
                SkillEnum.PRESS.v() + ":" + entry.press + SkillEnum.MS.v() +
                SkillEnum.RELEASE.v() + ":" + entry.release + SkillEnum.MS.v());
        return true;
    }

完整的程序 在我的github上，界面使用JavaFx，[剑灵灵剑卡刀脚本](https://github.com/yibierusi/blade-and-bns-macro)