---
title: IOS 系统通过plist方式安装App
date: 2019-02-14 17:26:46
updated: 2019-02-14 17:26:46
tags: [App,Java]
categories: IOS
description: "企业内部使用的app，是不会上架app store，那它们是如何升级的呢，本文主要介绍利用itms-services协议安装IPA安装包来完成app的升级更新功能。"
---
## 前言 ##
因为之前公司经营不善，几近破产，于是公司决定放弃互联网部门，然后我只能美滋滋的拿着违约补偿金，和平分手。
目前在新的公司做app后端开发，一期项目接近尾声，需要做一个App升级的功能，说穿了就是一个下载，这个东西90%的人大一就会吧，Android的本质就是下载，就不多做介绍。IOS大部分可以通过APP store升级更新，而小部分企业内部使用的app，是不会上架app store，那它们是如何升级的呢，本文主要介绍利用itms-services协议安装IPA安装包。

## 正文 ##
### 准备工作 ###
 1. SSL证书 
 2. 对应的plist文件 
 3. IPA安装包一个


在iOS开发中，Xcode 7.0也就是iOS9.0(以及以后)中开发中要求App内访问的网络必须使用HTTPS协议，以此来提高数据传输之间的安全性。所以我们需要支持Https的服务环境（也就是SSL证书）。

**注意：个人名义发布的或者非权威机构发布的证书是无用的。** 

当然如果没有有效的证书也可以使用别人的，比如[七牛云](https://www.jianshu.com)、[github](https://www.jianshu.com)、[coding](https://coding.net/)，把上面准备好的 plist 文件 与 IPA 安装包往上面一丢，完事。

### plist文件 ###
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
      <!-- array of downloads. -->
      <key>items</key>
      <array>
       <dict>
        <!-- an array of assets to download -->
         <key>assets</key>
          <array>
           <!-- software-package: the ipa to install. -->
            <dict>
             <!-- required. the asset kind. -->
              <key>kind</key>
              <string>software-package</string>
              <!-- optional. md5 every n bytes. will restart a chunk if md5 fails. -->
              <key>md5-size</key>
              <integer>10485760</integer>
              <!-- optional. array of md5 hashes for each "md5-size" sized chunk. -->
              <key>md5s</key>
              <array>
                <string>41fa64bb7a7cae5a46bfb45821ac8bba</string>
                <string>51fa64bb7a7cae5a46bfb45821ac8bba</string>
              </array>
              <!-- required. the URL of the file to download. -->
              <key>url</key>
              <string>https://www.example.com/apps/foo.ipa</string>
            </dict>
            <!-- display-image: the icon to display during download.-->
            <dict>
             <key>kind</key>
             <string>display-image</string>
             <!-- optional. indicates if icon needs shine effect applied. -->
             <key>needs-shine</key>
             <true/>
             <key>url</key>
             <string>https://www.example.com/image.57x57.png</string>
            </dict>
            <!-- full-size-image: the large 512x512 icon used by iTunes. -->
            <dict>
             <key>kind</key>
             <string>full-size-image</string>
             <!-- optional. one md5 hash for the entire file. -->
             <key>md5</key>
             <string>61fa64bb7a7cae5a46bfb45821ac8bba</string>
             <key>needs-shine</key>
             <true/>
             <key>url</key><string>https://www.example.com/image.512x512.jpg</string>
            </dict>
          </array>
    <key>metadata</key>
          <dict>
           <!-- required -->
           <key>bundle-identifier</key>
           <string>com.example.fooapp</string>
           <!-- optional (software only) -->
           <key>bundle-version</key>
           <string>1.0</string>
           <!-- required. the download kind. -->
           <key>kind</key>
           <string>software</string>
           <!-- optional. displayed during download; typically company name -->
           <key>subtitle</key>
           <string>Apple</string>
           <!-- required. the title to display during the download. -->
           <key>title</key>
           <string>Example Corporate App</string>
          </dict>
        </dict>
      </array>
    </dict>
    </plist>



**以下栏是必填项：**
URL：应用 (.ipa) 文件的完全限定 HTTPS URL
`display-image`：57 x 57 像素的 PNG 图像，在下载和安装过程中显示。指定图像的完全限定 URL
`full-size-image`：512 x 512 像素的 PNG 图像，表示 iTunes 中相应的应用
`bundle-identifier`：应用的包标识符，与 Xcode 项目中指定的完全一样
`bundle-version`：应用的包版本，在 Xcode 项目中指定
`title`：下载和安装过程中显示的应用的名称		

样本清单文件还包含可选键。例如，如果应用文件太大，并且想要在执行错误检验（TCP 通信通常会执行该检验）的基础上确保下载的完整性，可以使用 MD5 键。通过指定项目数组的附加成员，您还可以使用一个清单文件安装多个应用。

### 后端代码 ###
后端代码就是普通的下载，强行贴上来。

    @RequestMapping("/upgrade/{name}")
    private HttpServletResponse downloadFile(HttpServletResponse response, @PathVariable String name) {
        String fileName = name;//被下载的文件名，默认路径为/,
        try {
            // path是指欲下载的文件的路径。
            File file = new File(fileName);
            // 取得文件名。
            String filename = file.getName();
            // 取得文件的后缀名。
            String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();
            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(fileName));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setHeader("Content-Type","text/plain;charset=utf-8");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
            return null;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

所以我的plist里面的应用url是`http://127.0.0.1/upgrade/appname.ipa` (plist文件中的URL可以是https也可以是http)

### 下载应用 ###
确认通过浏览器能够直接访问到plist文件，及下载ipa文件后
用safari访问 以下链接即可下载应用(也可以通过此方法下载国区未上线的游戏)

    itms-services:///?action=download-manifest&url=https://127.0.0.1/upgrade/plistname.plist

### 参考文件 ###

 1. [IOS部署参考](https://help.apple.com/deployment/ios/#/apdda9e027d2)
